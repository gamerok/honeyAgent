package ru.nrgteam.medvdom.honeyagent.registration;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.Patterns;

import ru.nrgteam.medvdom.honeyagent.global.tools.connector.ItemDynamicKey;
import ru.nrgteam.medvdom.honeyagent.global.tools.connector.ItemPersonalKey;
import ru.nrgteam.medvdom.honeyagent.global.tools.connector.ServerConnector;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import io.realm.Realm;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;

/**
 * Created by gamerok on 23.03.17.
 * Набор статических паременных и функций помогающих при регистрации
 */

@SuppressWarnings("WeakerAccess")
public class RegistrationStatic {
    private static final String TAG = "RegistrationStatic";

    /**
     * mav - имя хранилища где будем хранить всё самое важное
     */
    public static final String MAIN_KEYS_PREFERENCES = "MainKeys_sharedPreferences";
    /**
     * mav - имя хранилища где будем хранить всё самое важное
     */
    public static final String MOBILE_PHONE_PREFERENCES = "MobilePhone_sharedPreferences";
    /**
     * mav - имя хранилища где будем хранить всё самое важное
     */
    public static final String PASSWORD_PREFERENCES = "Password_sharedPreferences";
    /**
     * mav - имя хранилища где будем хранить всё самое важное
     */
    public static final String DYNAMIC_KEY_PREFERENCES = "DynamicKey_sharedPreferences";
    /**
     * mav - имя хранилища где будем хранить всё самое важное
     */
    public static final String SURNAME_PREFERENCES = "Surname_sharedPreferences";
    /**
     * mav - имя хранилища где будем хранить всё самое важное
     */
    public static final String FIRST_NAME_PREFERENCES = "FirstName_sharedPreferences";
    /**
     * mav - имя хранилища где будем хранить всё самое важное
     */
    public static final String PATRONYMIC_PREFERENCES = "Patronymic_sharedPreferences";
    /**
     * mav - имя хранилища где будем хранить всё самое важное
     */
    public static final String EMAIL_PREFERENCES = "Email_sharedPreferences";
    /**
     * mav - Время между нажатиями на кнопку очистить
     */
    public static final String CLEAR_BUTTON_TIME = "ClearButtonTime";

    /**
     * mav - параметр устанавливающий промежутки между нажатиями на кнопку очистить
     */
    public static final int CLEAR_BUTTON_TIME_INTERVAL_MIN = 60;

    /**
     * mav - сохраняем персональный ключ
     */
    public static void savePersonalKey(String s, Context context) {
        try {
            //mav - собирём данные для сохранения в базу данных
            ItemPersonalKey itemPersonalKey = new ItemPersonalKey();

            itemPersonalKey.setKey(s);
            itemPersonalKey.setDataTime();

            //mav - сохраним в базу данных этот ключ
            //mav - инициализируем (по умолчанию)
            Realm realm = Realm.getInstance(context);


            //mav - если что-то было до этого в базе данных - удаляем и перезаписываем новые данные
            realm.beginTransaction();
            realm.where(ItemPersonalKey.class).findAll().clear();
            realm.copyToRealm(itemPersonalKey);
            realm.commitTransaction();

            //mav - для полного счастья нужно ключики переповерить и обновить
            new ServerConnector(context);

            realm.close();

        } catch (Exception e) {
            Log.e(TAG, "saveDynamicKey: ", e);
        }
    }


    /**
     * mav - сохраняем динамический ключ
     */
    public static void saveDynamicKey(ItemRegistration itemRegistration, Context context) {
        try {
            //mav - собирём данные для сохранения в базу данных
            ItemDynamicKey itemDynamicKey = new ItemDynamicKey();
            itemDynamicKey.setDateTime();
            // TODO: 14.03.17 после проверок надо поставить нормальный статус
            itemDynamicKey.setType(1);
            itemDynamicKey.setDateConnection();

            //mav - вычтим 2 дня от текущей даты, чтобы ускорить процесс обновления ключа
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(itemDynamicKey.getDateTime());
            calendar.add(Calendar.DATE, -3);
            itemDynamicKey.setDateTime(calendar.getTime());

            //mav - так же и время соединения подкрутим...
            itemDynamicKey.setDateConnection(calendar.getTime());

            itemDynamicKey.setKey(itemRegistration.getUUId());

            //mav - сохраним в базу данных этот ключ
            //mav - инициализируем (по умолчанию)
            Realm realm = Realm.getInstance(context);

            //mav - если что-то было до этого в базе данных - удаляем и перезаписываем новые данные
            realm.beginTransaction();
            realm.where(ItemDynamicKey.class).findAll().clear();
            realm.copyToRealm(itemDynamicKey);
            realm.commitTransaction();

            saveDynamicKeyShared(realm, context);
            realm.close();

        } catch (Exception e) {
            Log.e(TAG, "saveDynamicKey: ", e);
        }
    }

    /**
     * mav - сохраним динамический ключ во внутреннее хранилище
     */
    public static void saveDynamicKeyShared(Realm realm, Context context) {
        try {
            //mav - находим динамический ключ
            ItemDynamicKey itemDynamicKey = realm.where(ItemDynamicKey.class).findFirst();

            //mav - Доступ
            SharedPreferences sharedPreferences = context.getSharedPreferences(MAIN_KEYS_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(DYNAMIC_KEY_PREFERENCES, itemDynamicKey.getKey());
            editor.apply();

        } catch (Exception e) {
            Log.e(TAG, "saveDynamicKey: ", e);
        }
    }

    /**
     * mav - список действий на кнопку очистить
     */
    public static void clearRegistration(Context context) {
        try {
            // mav 2017.03.23 - удаляем запись с динамическим ключём из внутреннего хранилища
            SharedPreferences sharedPreferences = context.getSharedPreferences(MAIN_KEYS_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(DYNAMIC_KEY_PREFERENCES, "");
            editor.apply();

            // mav 2017.03.23 - удаляем запись из базы данных
            Realm realm = Realm.getInstance(context);
            realm.beginTransaction();
            realm.where(ItemRegistration.class).findAll().clear();
            realm.commitTransaction();
            realm.close();

        } catch (Exception e) {
            Log.e(TAG, "clearRegistration: ", e);
        }
    }

    /**
     * mav - Устанавливаем время нажатия на кнопку очистить
     */
    private static void setTimeClearButton(long l, Context context){
        try{
            // mav 2017.03.23 - сохраняем запись с временем нажатия на кнопку Очистить
            SharedPreferences sharedPreferences = context.getSharedPreferences(MAIN_KEYS_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(CLEAR_BUTTON_TIME, l);
            editor.apply();
        }catch (Exception e){
            Log.e(TAG, "setTimeClearButton: ", e);
        }
    }

    /**
     * mav - сохраняем данные в внутренее хранилище
     *
     * @param calendar - значение для сохранения
     */
    private static void setTimeClearButton(Calendar calendar, Context context) {
        try {
            long l = calendar.getTimeInMillis();
            setTimeClearButton(l, context);
        } catch (Exception e) {
            Log.e(TAG, "setTimeClearButton: ", e);
        }
    }

    /**
     * mav - сохраняем данные в внутренее хранилище с текущим временем
     */
    public static void setTimeClearButton(Context context) {
        try {
            //mav - получаем текущее время
            Calendar calendarTime = Calendar.getInstance();
            calendarTime.setTime(new Date());
            setTimeClearButton(calendarTime, context);
        } catch (Exception e) {
            Log.e(TAG, "setTimeClearButton: ", e);
        }
    }

    /**
     * mav - читаем время из хранилища
     *
     * @return кол-во миллисекунд
     */
    private static long getTimeClearButtonLong(Context context) {
        long l = 0;
        try {
            // mav 2017.03.23 - сохраняем запись с временем нажатия на кнопку Очистить
            SharedPreferences sharedPreferences = context.getSharedPreferences(MAIN_KEYS_PREFERENCES, Context.MODE_PRIVATE);
            l = sharedPreferences.getLong(CLEAR_BUTTON_TIME, 0);
        } catch (Exception e) {
            Log.e(TAG, "getHelpMessageTimeLong: ", e);
        }
        return l;
    }

    /**
     * mav - читаем время из хранилища
     */
    public static Calendar getTimeClearButtonCalendar(Context context) {
        Calendar calendar = null;
        try {
            long l = getTimeClearButtonLong(context);

            calendar = Calendar.getInstance();
            calendar.setTimeInMillis(l);

        } catch (Exception e) {
            Log.e(TAG, "getHelpMessageTimeCalendar: ", e);
        }
        return calendar;
    }

}

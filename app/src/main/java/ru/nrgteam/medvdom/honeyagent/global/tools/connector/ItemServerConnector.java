package ru.nrgteam.medvdom.honeyagent.global.tools.connector;

/**
 * Created by gamerok on 15.03.17.
 * класс в котором будет храниться информация для подключения к серверу
 */

@SuppressWarnings("WeakerAccess")
public class ItemServerConnector {

    /**
     * mav - статический ключ
     */
    private String Authorization;

    /**
     * mav - имя основного скрипта
     */
    private String MainAddress;
    /**
     * mav - ключ владельца
     */
    private String Owner;
    /**
     * mav - динамический ключ
     */
    private String DynamicKey;
    /**
     * mav - имя внутреннего скрипта
     */
    private String Link;

    /**
     * mav - значение заголовка "Content-Disposition" - заголовок в котором можно дополнительные параметры вносить <p>("owner="bbbb"fileName="ilc_agent-4.apk".")
     */
    private String ContentDisposition;
    /**
     * mav - Тело запроса <p>(Текст)
     */
    private String Body;
    /**
     * mav - результат работы
     */
    private String Result;
    /**
     * mav - тип сервера <p>(1 - тест, 2 - основные)
     */
    private int ServerType;
    /**
     * mav - значение заголовка "Content-Type"
     */
    private String ContentType ;

    /**
     * mav - константа для ответа по умалчанию
     */
    @SuppressWarnings("FieldCanBeLocal")
    public static String ERROR_CONNECTOR = "ERROR_CONNECTOR";

    /**
     * mav - инициализация класса
     */
    public ItemServerConnector() {
        clear();
    }

    /**
     * mav - инициализация класса
     */
    public ItemServerConnector(ItemServerConnector itemServerConnector) {
        clear();

        setOwner(itemServerConnector.getOwner());
        setDynamicKey(itemServerConnector.getDynamicKey());

        setLink(itemServerConnector.getLink());
        setContentDisposition(itemServerConnector.getContentDisposition());
        setBody(itemServerConnector.getBody());
        setServerType(itemServerConnector.getServerType());
    }

    /**
     * mav - очищаем всё не нужное
     */
    private void clear() {
        Authorization = "e121ae24-7c83-401d-abe3-8ca8e5d42283"; //mav - чёрный агент
        MainAddress = "access-003";
        Owner = "00000000-0000-0000-0000-000000000001";
        DynamicKey = "00000000-0000-0000-0000-000000000002";
        Link = "";
        ContentDisposition = "";
        Body = "";
        Result = ERROR_CONNECTOR;
        ServerType = 0;
        ContentType = "application/json; charset=utf-8";
    }

    /**
     * mav - инициализация класса
     */
    public ItemServerConnector(String sLink, String sContentDisposition, String sBody, int iServerType) {
        clear();

        setLink(sLink);
        setContentDisposition(sContentDisposition);
        setBody(sBody);
        setServerType(iServerType);
    }

    /**
     * mav - Читаем - статический ключ <p>("29165661-ea38-4919-bfc6-255bca479e77")
     */
    public String getAuthorization() {
        return Authorization;
    }

    /**
     * mav - Читаем - имя основного скрипта <p>("access-001")
     */
    public String getMainAddress() {
        return MainAddress;
    }

    /**
     * mav - Устанавливаем - ключ владельца <p>("29165661-ea38-4919-bfc6-255bca479e77")
     */
    public void setOwner(String owner) {
        if (owner != null && owner.length() == 36)
            Owner = owner;
    }

    /**
     * mav - Читаем - ключ владельца <p>("29165661-ea38-4919-bfc6-255bca479e77")
     */
    public String getOwner() {
        return Owner;
    }

    /**
     * mav - Устанавливаем - динамический ключ <p>("29165661-ea38-4919-bfc6-255bca479e77")
     */
    public void setDynamicKey(String dynamicKey) {
        if (dynamicKey != null && dynamicKey.length() == 36)
            DynamicKey = dynamicKey;
    }

    /**
     * mav - Читаем - динамический ключ <p>("29165661-ea38-4919-bfc6-255bca479e77")
     */
    public String getDynamicKey() {
        return DynamicKey;
    }

    /**
     * mav - Устанавливаем - имя внутреннего скрипта <p>("genDynamicKey-001")
     */
    public void setLink(String link) {
        if (link != null && link.length() > 0)
            Link = link;
    }

    /**
     * mav - Читаем - имя внутреннего скрипта <p>("genDynamicKey-001")
     */
    public String getLink() {
        return Link;
    }

    /**
     * mav - Устанавливаем - значение заголовка "Content-Disposition" - заголовок в котором можно дополнительные параметры вносить <p>("owner="bbbb"fileName="ilc_agent-4.apk".")
     */
    public void setContentDisposition(String contentDisposition) {
        if (contentDisposition != null && contentDisposition.length() > 0)
            ContentDisposition = contentDisposition;
    }

    /**
     * mav - Читаем - значение заголовка "Content-Disposition" - заголовок в котором можно дополнительные параметры вносить <p>("owner="bbbb"fileName="ilc_agent-4.apk".")
     */
    public String getContentDisposition() {
        return ContentDisposition;
    }

    /**
     * mav - Устанавливаем - Тело запроса <p>(Текст)
     */
    public void setBody(String body) {
        if (body != null && body.length() > 0)
            Body = body;
    }

    /**
     * mav - Читаем - Тело запроса <p>(Текст)
     */
    public String getBody() {
        return Body;
    }

    /**
     * mav - Устанавливаем - результат работы
     */
    public void setResult(String result) {
        if (result != null && result.length() > 0)
            Result = result;
    }

    /**
     * mav - Читаем - результат работы
     */
    public String getResult() {
        return Result;
    }


    /**
     * mav - Устанавливаем - тип сервера <p>(1 - тест, 2 - основные)
     */
    public void setServerType(int serverType) {
        if (serverType > 0)
            ServerType = serverType;
    }

    /**
     * mav - Читаем - тип сервера <p>(1 - тест, 2 - основные)
     */
    public int getServerType() {
        return ServerType;
    }

    /**
     * mav - Читаем - значение заголовка "Content-Type" <p>("application/json; charset=utf-8")
     */
    public String getContentType() {
        return ContentType;
    }
}

package ru.nrgteam.medvdom.honeyagent.global.tools;

/**
 * Created by gamerok on 30.11.16.
 * mav - класс для хранения всяких штук для подключения к нашим серверам
 */

public class Con extends ConT{

    /**
     * mav - Список БОЕВЫХ серверов для подключения
     */
    private static String rootServersName[] = {
//            "root_ilc_old.connexus.ru:1080"
//            , "test_ilc_new.connexus.ru:2080"
//            , "root_ilc_old.connexus.ru:7080"
//            , "test_ilc_new.connexus.ru:8080"
//            , "plan_ilc_test.connexus.ru:11080"
//            , "plan_ilc_test.connexus.ru:12080"
            "62.141.96.11:1080"
            , "5.8.177.170:2080"
            , "62.141.96.11:7080"
            , "5.8.177.170:8080"
            , "178.218.119.162:11080"
            , "178.218.119.162:12080"
    };

    /**
     * mav - номер последнего использованного БОЕВОГО сервера
     */
    private static volatile int CurrentRootServerNumber = 0;

    /**
     * mav - получение следующего по списку имя ТЕСТОВОГО сервера с переключением номера сервера
     */
    public static String getNextRootServersName(int connectionNumber) {
        if (connectionNumber <= rootServersName.length) {
            changeRootServer();
            return rootServersName[CurrentRootServerNumber];
        }else{
            return getNextTestServersName();
        }
    }

    /**
     * mav - определяем количество БОЕВЫХ (+тестовые) серверов для поиска
     */
    public static int getCountRootServers() {
        return getCountTestServers() + rootServersName.length;
    }

    /**
     * mav - меняем БОЕВОЙ сервер на следующий по порядку
     */
    private static void changeRootServer() {
        CurrentRootServerNumber = (CurrentRootServerNumber + 1) % rootServersName.length;
    }

}

package ru.nrgteam.medvdom.honeyagent.global.tools.connector;

import android.content.Context;
import android.util.Log;

import ru.nrgteam.medvdom.honeyagent.global.staticMethods.SF;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;

import org.json.JSONObject;

/**
 * Created by gamerok on 17.03.17.
 * Класс который будет проверять персональный статус, и в случае необходимости деактивировать приложение
 */

public class PersonalStatus {
    private static final String TAG = "PersonalStatus";

    /**
     * mav - собственно инициализация обработки и проверка статуса
     */
    public PersonalStatus(Context context) {
        //mav - проверим на то что есть ли у нас регистрация
        if (ServerConnector.PersonalKey.length() == 0)
            return;

        Thread thread = new Thread(() -> {
            try {

                //mav - откладываем проверку на секунду - чтобы пока не конфликтовал с получением динамического ключа, который написан в MainActivity.onCreate
                SF.SleepSeconds(1);

                String sLink = "bestCredits-004/fn_local_get_personal_status_002";
                //mav - тут кэширование не используем... т.е. блокируем сразу, а не через какое-то время
                String sContentDisposition = "duration_cache=\"0\".";
                String sBody = getJsonFromParams();

                // TODO: 07.04.17 Перенести на боевые сервера
                //mav - спрашиваем у сервера - какой статус персонального ключа
                String sAnswer = ServerConnector.getResult(sLink, sContentDisposition, sBody, 2);

                if (sAnswer.contains("SUCCESSFULLY")) {
                    JSONObject jsonObject = new JSONObject(sAnswer);
                    if ("SUCCESSFULLY".equals(jsonObject.get("scriptResponse")) && "true".equals(jsonObject.get("f_result"))) {

                        //mav - если всё прекрасно, то получаем статус ключа
                        String sStatusKey = (String) jsonObject.get("status_key");
                        int iStatusKey = Integer.valueOf(sStatusKey);

                        //maм - если статус ключе равен 1, значит пришло время удалить приложение.
                        //mav - бывают случаи когда таблицы обновляют... то в ней нет данных.
                        //mav - в таких случаях доступ к серверам не получить и динамический ключ
                        if (iStatusKey == 1) {
                            Logs.wtf(TAG, "DELETE APPLICATIONS");

                            //mav - даём приложению 10 секунд чтобы отправить лог об этом удалении к нам на сервер.
                            SF.SleepSeconds(10);

                            //mav - собственно само удаление приложение
                            ServerConnector.deletePersonalKey(context);
                        }
                    }
                }

//                //todo - но эту проверку надо перенести в некий сервис, чтобы эта проверка запускалась например рас в час.
//                //mav - дополнительно делаем провеку динамического ключа при обновлении формы
//                ServerConnector.getDynamicKey(context);

            } catch (Exception e) {
                Logs.e(TAG, "PersonalStatus: ", e);

            }
        });
        thread.setPriority(1);  //mav - минимальный приоритет
//mav - чтобы не умерал вместе с родителем - комментируем эту строчку
//        thread.setDaemon(true);
        thread.start();
    }

    /**
     * mav - собираем JSON
     */
    private static String getJsonFromParams() {
        String s;
        JSONObject jsonObject = new JSONObject();
        try {
            //mav - т.к. теперь не нужно отправлять имя функции в боди, то собираем пустой JSON
//            jsonObject.put("function_name", "fn_local_get_personal_status_002");
            s = jsonObject.toString();
        } catch (Exception e) {
            Logs.e(TAG, "getJsonFromParams: ", e);
            s = "";
        }
        return s;
    }
}

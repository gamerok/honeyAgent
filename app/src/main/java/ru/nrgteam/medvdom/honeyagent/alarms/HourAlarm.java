package ru.nrgteam.medvdom.honeyagent.alarms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ru.nrgteam.medvdom.honeyagent.mainService.MainServiceStatic;

/**
 * Created by gamerok on 06.06.17.
 * Будильник на ежечастное срабатывание, для проверки динамического ключа.
 */

public class HourAlarm extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.sendBroadcast(new Intent(MainServiceStatic.MAIN_SERVICE_HOUR_ALARM));
    }

}

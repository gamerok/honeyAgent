package ru.nrgteam.medvdom.honeyagent.registration;

import android.util.Log;

import ru.nrgteam.medvdom.honeyagent.global.staticMethods.SF;
import ru.nrgteam.medvdom.honeyagent.global.tools.Con;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by gamerok on 01.03.17.
 * получение персонального ключа
 */
@SuppressWarnings("WeakerAccess")
public class PersonalKey {
    private static final String TAG = "PersonalKey";

    /**
     * mav - Класс модели обработки биндинга
     */
    private ItemRegistration mItemRegistration;

    /**
     * mav - инициализация класса
     * @param itemRegistration  класс с записями для регистрации
     */
    public PersonalKey(ItemRegistration itemRegistration) {
        mItemRegistration = itemRegistration;
    }

    /**
     * mav - стандартная функция получения персонального ключа
     */
    public String getPersonalKey() {
        return getPersonalKey(1);
    }

    /**
     * mav - отправка данных
     *
     * @param countConnections номер попытки дя подключения
     */
    public String getPersonalKey(int countConnections) {
        if (countConnections > Con.getCountTestServers()) {
            return "ERROR_countConnections";
        }

        //mav - получаем имя следующего сервера
        // TODO: 26.04.17 перенести на боевые
        final String sServerName = Con.getNextTestServersName(countConnections);

        //mav - строка для получения ответа от сервера
        String s = "";

//        Log.i(TAG, "run: " + "Start " + "getPersonalKey (" + countConnections + ", " + sServerName + ")");

        //mav - куда будем отправлять
        String STRING_URL = "http://" + sServerName + "/" + "getPersonalKey-002" + "/";

        HttpURLConnection httpURLConnection = null;
        try {

            //mav - для начало подготовим URL
            URL url = new URL(STRING_URL);

            //mav - создаём соединение
            httpURLConnection = (HttpURLConnection) url.openConnection();

            //mav - говорим что это POST
            httpURLConnection.setRequestMethod("POST");

            //mav - устанавливаем заголовок для кодировки
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            //mav - устанавливаем заголовок для авторизации
            httpURLConnection.setRequestProperty("Authorization", "9eab9d86-001f-47bb-b939-c2918cf74140");
            //mav - устанавливаем заголовок для персонального ключа
            httpURLConnection.setRequestProperty("Owner", "7" + mItemRegistration.getMobilePhone());
            //mav - устанавливаем заголовок для динамического ключа
            httpURLConnection.setRequestProperty("Dynamic-key", mItemRegistration.getUUId());

            //mav - нужно указать тело сообщения
            OutputStream outputStream = httpURLConnection.getOutputStream();

            //mav - вставляем нашу фотку
            outputStream.write(mItemRegistration.getPassword().getBytes());
            outputStream.close();

            //mav - и пробуем отправить
            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());

            //начитываем ответ
            s = SF.readStream(inputStream);

//            Log.v(TAG, "getPersonalKey: " + s);

        } catch (Exception e) {
            Log.w(TAG, "getPersonalKey: ", e);
        } finally {
            if (httpURLConnection != null) {
                //Закрываем соединение
                httpURLConnection.disconnect();
            }
        }

        //mav - если ответ положительный то пытаемся прикрепить фотку
        if (s.contains("SUCCESSFULLY")) {

            try {
                JSONObject jsonObject = new JSONObject(s);

                s = (String) jsonObject.get("personalUUId");

            } catch (Exception e) {
                Log.e(TAG, "getPersonalKey: ", e);
            }


        } else if (s.contains("ERROR_PERMISSION_NO-ACCESS")){
            //mav - если хотябы от одного сервера пришло "такие ключи не подходят" - значит выдаём ответ "нет досупа"
        } else {
            s = getPersonalKey(countConnections + 1);
        }

        return s;
    }



}

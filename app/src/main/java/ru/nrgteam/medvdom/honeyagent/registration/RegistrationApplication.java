package ru.nrgteam.medvdom.honeyagent.registration;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONObject;

import io.realm.Realm;
import ru.nrgteam.medvdom.honeyagent.R;
import ru.nrgteam.medvdom.honeyagent.databinding.AMainBinding;
import ru.nrgteam.medvdom.honeyagent.global.staticMethods.SF;
import ru.nrgteam.medvdom.honeyagent.global.tools.PercentDialog;
import ru.nrgteam.medvdom.honeyagent.global.tools.connector.ItemDynamicKey;
import ru.nrgteam.medvdom.honeyagent.global.tools.connector.ItemPersonalKey;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;
import ru.nrgteam.medvdom.honeyagent.mainService.MainServiceStatic;

/**
 * Created by gamerok on 23.03.17.
 * mav - главный обработчик действий при нашей регистрации
 * mav 2017.04.13 - Внёс исправления связанные с багом при нажатии на кнопку "очистить"
 */
public class RegistrationApplication {
    private static final String TAG = "RegistrationApplication";

    /**
     * mav - сам биндинг
     */
    private AMainBinding mBinding;

    /**
     * mav - активити
     */
    private Activity mActivity;

    /**
     * mav - контекст
     */
    private Context mContext;

    /**
     * mav - Класс для хранения данных при первоночальной регистрации
     */
    private ItemRegistration mItemRegistration;

    /**
     * mav - Класс модели обработки биндинга
     */
    private RegistrationModel mRegistrationModel;

    /**
     * mav - текущая версия приложения
     */
    @SuppressWarnings("WeakerAccess")
    public static int currentVersion = 0;

    /**
     * mav - диалоговое окно с процентами
     */
    private PercentDialog mPercentDialog;


    /**
     * mav - инициализация всего что связано с регистрацией
     */
    public RegistrationApplication(AMainBinding binding, Activity activity) {
        mBinding = binding;
        mActivity = activity;
        mContext = mActivity.getApplicationContext();
//
//        //mav - коль работаем с подключением, то инициализируем такую штуку
//        new ServerConnector(mContext);


        // mav 2017.02.22 - создаём класс для сбора информации
        mItemRegistration = new ItemRegistration();

        // mav: 12.04.17 походу эти строчку нужно переопределять когда мы данные подгружаем из базы данных
        // mav 2017.02.22 - создаём модель по обработки изменений на форме
        mRegistrationModel = new RegistrationModel(mItemRegistration);

        // mav 2017.02.22 - соединяем форму с обработкой
        binding.setRegistration(mRegistrationModel);

        //mav - инициализируем кнопки
        onClickInit();

        //mav - проверка прошли ли мы регистрацию приложения
        checkSending();

        //mav - версия программы
        if (currentVersion == 0) {
            try {
                currentVersion = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                Logs.e(TAG, "RequestConnection: ", e);
            }
        }

        //mav - инициализация диалогового окна с процентами
        mPercentDialog = new PercentDialog(100, "Отправляю письмо...", activity);

    }

    /**
     * mav - проверяем есть ли в базе данных попытки отправить данные к нам на сервер.
     */
    private void checkSending() {
        Thread thread = new Thread(() -> {
            try {
                //mav - инициализируем (по умолчанию)
                Realm realm = Realm.getInstance(mContext);

                long l = realm.where(ItemRegistration.class).count();

                //mav - если в базе ничего нет, проверим в SharedPreference
                if (l == 0) {
                    l = loadKeys(realm);
                }

                //mav - если в базе что-то появилось, то отображаем соответствующие кнопки
                if (l > 0) {
                    //mav - теперь проверим есть ли динамический и персональные ключи
                    long l_ItemDynamicKey = realm.where(ItemDynamicKey.class).count();
                    long l_ItemPersonalKey = realm.where(ItemPersonalKey.class).count();

                    //mav - если есть то скроем всё...
                    if (l_ItemDynamicKey > 0 && l_ItemPersonalKey > 0) {
                        //mav отобразим кнопку проверки, а остальное прячем
                        mActivity.runOnUiThread(() -> showStepRegistrations(3));

                    } else {
                        //mav - если ключей нет, то кнопку "проверить" оставим
                        mActivity.runOnUiThread(() -> showStepRegistrations(2));

                        //mav - если поля с регистрацией пустые и мы зашли ещё раз в приложение, то правильно будет заполнить эти данные
                        ifEmptyItemRegistration();
                    }
                } else {
                    //mav - если ключей нет, то откроем поля для ввода
                    mActivity.runOnUiThread(() -> showStepRegistrations(1));
                }

                //mav - закрываем базу за собой
                realm.close();

            } catch (Exception e) {
                Log.e(TAG, "checkSending: ", e);
            }
        });
        thread.setPriority(1);  //mav - минимальный приоритет
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * mav - если класс с данными для регистрации пустой, то заполним его тем что есть у нас в базе данных
     */
    private void ifEmptyItemRegistration() {
        //mav - если данные класс не пустой то перезаписывать его не надо
        if (mItemRegistration.getJSON().length() != 0)
            return;

        //mav - если он всё же пустой, то прочитаем данные из базы данных
        //mav - проверим на миграцию базу данных
        Realm realm = Realm.getInstance(mContext);

        //mav - читаем данные из базы
        //mav - собственно получение данных из базы данных
        //mav - чтобы он был различим в любых потоках оборачиваем его в некий хитрый метод
        mItemRegistration = realm.copyFromRealm(realm.where(ItemRegistration.class).findFirst());

        //mav - благодоря обёртке copyFromRealm закрыть доступ к базе данных можно уже здесь
        realm.close();

        // mav: 12.04.17 походу эти строчку нужно переопределять когда мы даннве подгружаем в базе данных
        // mav 2017.02.22 - создаём модель по обработки изменений на форме
        mRegistrationModel = new RegistrationModel(mItemRegistration);

        // mav: 12.04.17 ПРОВЕРИТЬ
        // mav 2017.02.22 - соединяем форму с обработкой
        mBinding.setRegistration(mRegistrationModel);
    }

    /**
     * mav - отображение того или иного шага регистрации
     *
     * @param iStep номер шага <p>(1 - ввод данных, <p>2 - проверка данных, <p>3 - отображение основных кнопок)
     */
    private void showStepRegistrations(int iStep) {
        mBinding.llCheck.setVisibility(View.GONE);
        mBinding.llInput.setVisibility(View.GONE);
        mBinding.llMain.setVisibility(View.GONE);

        if (iStep == 1)
            mBinding.llInput.setVisibility(View.VISIBLE);
        else if (iStep == 2)
            mBinding.llCheck.setVisibility(View.VISIBLE);
        else if (iStep == 3)
            mBinding.llMain.setVisibility(View.VISIBLE);

    }

    /**
     * mav - функция автоматическиго восстановления ключей при миграции базы данных
     *
     * @return если данные получены - то ставим идиничку
     */
    private long loadKeys(Realm realm) {
        long l = 0;
        try {

            //mav - Доступ
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(RegistrationStatic.MAIN_KEYS_PREFERENCES, Context.MODE_PRIVATE);

            //mav - важные данные
            long MobilePhone = sharedPreferences.getLong(RegistrationStatic.MOBILE_PHONE_PREFERENCES, -1);
            String Password = sharedPreferences.getString(RegistrationStatic.PASSWORD_PREFERENCES, "");
            String DynamicKey = sharedPreferences.getString(RegistrationStatic.DYNAMIC_KEY_PREFERENCES, "");

            //mav - не очень важные данные
            String Surname = sharedPreferences.getString(RegistrationStatic.SURNAME_PREFERENCES, "");
            String FirstName = sharedPreferences.getString(RegistrationStatic.FIRST_NAME_PREFERENCES, "");
            String Patronymic = sharedPreferences.getString(RegistrationStatic.PATRONYMIC_PREFERENCES, "");
            String Email = sharedPreferences.getString(RegistrationStatic.EMAIL_PREFERENCES, "");

            //mav - если там были необходимые данные то вставим их в нашу новую базу
            if (MobilePhone > 0 && DynamicKey.length() > 0) {
                mItemRegistration.setMobilePhone(MobilePhone);
                mItemRegistration.setPassword(Password);
                mItemRegistration.setUUId(DynamicKey);

                mItemRegistration.setSurname(Surname);
                mItemRegistration.setFirstName(FirstName);
                mItemRegistration.setPatronymic(Patronymic);
                mItemRegistration.setEmail(Email);

                //mav - и заполним поля ввода при регистрации этими данными
                mRegistrationModel.notifyChange();

                //mav - перезаписываем данные
                realm.beginTransaction();
                realm.copyToRealm(mItemRegistration);
                realm.commitTransaction();

                //mav - указываем что одна запись там всё же появилась
                l = 1;

                //mav - проверим есть ли активация данного приложения
                checkInTread();

            }

        } catch (Exception e) {
            Log.e(TAG, "saveDynamicKey: ", e);
        }
        return l;
    }

    /**
     * mav - инициализируем кнопки и из обработку
     */
    private void onClickInit() {

        //mav - отправка заполненных данных
        mBinding.btnSend.setOnClickListener(view -> btnSendOnClickListener());

        // mav 2017.03.18 - проверка заполненных данных
        mBinding.btnCheck.setOnClickListener(view -> btnCheckOnClickListener());

        // mav 2017.03.23 - Кнопка очистить
        mBinding.btnClear.setOnClickListener(view -> onClickClear());
    }

    /**
     * mav - обработчик нажатия на кнопку очистить
     */
    private void onClickClear() {
        if (SF.funCheckTimer(RegistrationStatic.CLEAR_BUTTON_TIME_INTERVAL_MIN, RegistrationStatic.getTimeClearButtonCalendar(mContext))) {
            //mav - удаляем динамический ключ и данные из базы данных
            RegistrationStatic.clearRegistration(mContext);
            //mav - после очистки данных проверяем какие поля нужно отобразить пользователю
            checkSending();
            //mav - запоминаем время нажатия на кнопку очистить
            RegistrationStatic.setTimeClearButton(mContext);
        } else {
            String s = "Интервал между очисткой данных составляет 60 минут.";

            //mav - диалоговое окно перед отправкой
            new AlertDialog.Builder(
                    mActivity)
                    .setTitle("Обратите внимание!")
                    .setMessage(s)
                    .setCancelable(false) //mav - чтобы сообщение не закрывалось при клике на свободное место
                    .setNegativeButton(R.string.all_close, null)
                    .create()
                    .show();
        }
    }

    /**
     * mav - обработчик нажатия на кнопку отправки
     */
    private void btnSendOnClickListener() {
        try {
            String sErrorText = mRegistrationModel.inputValidation();
            if (sErrorText.length() > 0)
                Toast.makeText(mActivity, sErrorText, Toast.LENGTH_SHORT).show();
            else {

                //mav - прячем клавиатуру
                SF.funHideKeyboard(mBinding.etPassword, mContext);

                //mav - Сообщение для отправки
                String s = "После отправки этих данных наш оператор свяжется с вами и активирует это приложение.";

                //mav - диалоговое окно перед отправкой
                new AlertDialog.Builder(
                        mActivity)
                        .setTitle("Обратите внимание!")
                        .setMessage(s)
                        .setPositiveButton("ОТПРАВИТЬ", (dialog, which) -> sendFunction())
                        .setNegativeButton("Отмена", null)
                        .create()
                        .show();
            }
        } catch (Exception e) {
            Log.e(TAG, "btnSendOnClickListener: ", e);
        }
    }

    /**
     * mav - список действий при нажатии на кнопку Отправить
     */
    private void sendFunction() {
        try {
            // mav 2017.03.01 - собираем JSON
            String s = mRegistrationModel.toJSON();

            //mav - отправляем
            sendEmailInScript(s);

        } catch (Exception e) {
            Log.e(TAG, "sendFunction: ", e);
        }

    }

    /**
     * mav - сохранение данныех после успешной отправки
     */
    private void saveRegData() {
        try {
            //mav отобразим кнопку проверки, а остальное прячем
            showStepRegistrations(2);

            //mav - сохраняем в базу данных
            Realm realm = Realm.getInstance(mContext);

            //mav - если что-то было до этого в базе данных - удаляем и перезаписываем новые данные
            realm.beginTransaction();
            realm.where(ItemRegistration.class).findAll().clear();
            realm.copyToRealm(mItemRegistration);
            realm.commitTransaction();

            realm.close();

            //mav - Доступ
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(RegistrationStatic.MAIN_KEYS_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            //mav - запись данных
            editor.putLong(RegistrationStatic.MOBILE_PHONE_PREFERENCES, mItemRegistration.getMobilePhone());
            editor.putString(RegistrationStatic.PASSWORD_PREFERENCES, mItemRegistration.getPassword());

            //mav - эту строчку выключаем, мол она будет мешать логике... долго объяснять!
//            editor.putString(DYNAMIC_KEY_PREFERENCES, mItemRegistration.getUUId());

            editor.putString(RegistrationStatic.SURNAME_PREFERENCES, mItemRegistration.getSurname());
            editor.putString(RegistrationStatic.FIRST_NAME_PREFERENCES, mItemRegistration.getFirstName());
            editor.putString(RegistrationStatic.PATRONYMIC_PREFERENCES, mItemRegistration.getPatronymic());
            editor.putString(RegistrationStatic.EMAIL_PREFERENCES, mItemRegistration.getEmail());

            editor.apply();

        } catch (Exception e) {
            Log.e(TAG, "saveRegData: ", e);
        }
    }


    /**
     * mav - будем отправлять почту через наш скрипт на сервере CRM
     */
    private void sendEmailInScript(String sTextMessage) {

        //mav - показываем диалог
        mPercentDialog.showProgressDialog();

        Thread thread = new Thread(() -> sendEmailInScriptInTread(sTextMessage));
        thread.setPriority(10);  //mav - максимальный приоритет
//        thread.setDaemon(true);   //mav - умри но сделай!
        thread.start();
    }

    /**
     * mav - обработаем в потоке отправку письма
     */
    private void sendEmailInScriptInTread(String sTextMessage) {
        try {
            //mav - запускаем саму обработку отправки сообщения
            String sAnswer = RegistrationSender.getResult(sTextMessage);

            if (sAnswer.contains("SUCCESSFULLY")) {
                JSONObject jsonObject = new JSONObject(sAnswer);
                if ("SUCCESSFULLY".equals(jsonObject.get("scriptResponse"))
                        && "Sent".equals(jsonObject.get("resultSendingMessage"))) {

                    //mav - тогда отобразим соответствующую кнопку
                    mActivity.runOnUiThread(this::saveRegData);
                }
            }

            //mav - закрываем диалог
            mPercentDialog.closeDialog();

        } catch (Exception e) {
            Log.e(TAG, "sendEmailInScriptInTread: ", e);
        }
    }


    /**
     * mav - обработчик нажатия на кнопку проверки регистрации
     */
    private void btnCheckOnClickListener() {
        String s = "После того как с вами свяжется наш оператор, вы сможете проверить активацию этой программы. \nЕсли активация пройдёт успешно, то кнопка \"ПОДКЛЮЧИТЬСЯ\" пропадёт. \nИ вы сможете пользоваться этим приложением.";

        //mav - диалоговое окно перед проверкой
        new AlertDialog.Builder(
                mActivity)
                .setTitle("Обратите внимание!")
                .setMessage(s)
                .setPositiveButton("ПОДКЛЮЧИТЬСЯ", (dialog, which) -> checkFunctions())
                .setNegativeButton("Отмена", null)
                .create()
                .show();


    }

    /**
     * mav - список действий на кнопку проверить!
     */
    private void checkFunctions() {
        //mav - чтобы несколько раз не нажимали сразу
        mBinding.btnCheck.setTextColor(Color.GRAY);
        mBinding.btnCheck.setEnabled(false);

        Thread thread = new Thread(this::checkInTread);
        thread.setPriority(10);  //mav - максимальный приоритет
        thread.setDaemon(true);
        thread.start();
    }


    /**
     * mav - функция запускаемая в параллельном потоке для проверки доступа
     */
    private void checkInTread() {
        try {

            //mav - проверим на миграцию базу данных
            Realm realm = Realm.getInstance(mContext);

            //mav - читаем данные из базы
            //mav - собственно получение данных из базы данных
            ItemRegistration itemRegistration = realm.copyFromRealm(realm.where(ItemRegistration.class).findFirst());

            //mav - закрываемся для базы данных
            realm.close();

            //mav - готовимся к получению персонального ключа
            PersonalKey personalKey = new PersonalKey(itemRegistration);

            String s = personalKey.getPersonalKey();

            if (s.contains("ERROR_")) {
                mActivity.runOnUiThread(() -> {
                    Toast.makeText(mActivity, "Доступ не получен, код сообщения (" + s + ")", Toast.LENGTH_SHORT).show();
                    //mav - активируем кнопку обратно
                    mBinding.btnCheck.setTextColor(Color.WHITE);
                    mBinding.btnCheck.setEnabled(true);

                });

            } else {

                //mav - сохраним динамический ключ
                RegistrationStatic.saveDynamicKey(itemRegistration, mContext);

                //mav - сохраним динамический ключ и загрузим их в коннектор
                RegistrationStatic.savePersonalKey(s, mContext);

                mActivity.runOnUiThread(() -> {

                    Toast.makeText(mActivity, "Доступ получен!", Toast.LENGTH_SHORT).show();

                    //mav - отображаем основные кнопки
                    showStepRegistrations(3);

                    // TODO: 14.03.17 mav - что-то полезное - например старт сервиса
                    MainServiceStatic.checkServices(true, mActivity);


                });
            }


        } catch (Exception e) {
            Log.e(TAG, "btnCheckOnClickListener: ", e);
        }
    }
}

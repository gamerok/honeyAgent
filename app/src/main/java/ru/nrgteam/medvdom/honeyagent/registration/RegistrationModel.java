package ru.nrgteam.medvdom.honeyagent.registration;


import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.Color;

import ru.nrgteam.medvdom.honeyagent.BR;

/**
 * Created by gamerok on 22.02.17.
 * Класс модель для формы регистрации
 */

@SuppressWarnings("UnusedParameters")
public class RegistrationModel extends BaseObservable {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    /**
     * mav - Класс содержимого
     */
    private ItemRegistration mItemRegistration;

    // mav 2017.02.22 - инициализация переменных этого класса
    @SuppressWarnings("WeakerAccess")
    public RegistrationModel(ItemRegistration itemRegistration) {
        mItemRegistration = itemRegistration;
    }

    /**
     * mav - Читаем - значение поля Фамилия <p>("Мятечкин")
     */
    @Bindable
    public String getSurname() {
        return mItemRegistration.getSurname();
    }

    /**
     * mav - Читаем - значение поля Имя <p>("Александр")
     */
    @Bindable
    public String getFirstName() {
        return mItemRegistration.getFirstName();
    }

    /**
     * mav - Читаем - значение поля Отчество <p>("Викторович")
     */
    @Bindable
    public String getPatronymic() {
        return mItemRegistration.getPatronymic();
    }

    /**
     * mav - Читаем - значение поля Мобильный телефон <p>("9103089229")
     */
    @Bindable
    public String getMobilePhone() {
        if (mItemRegistration.getMobilePhone() > 0)
            return String.valueOf(mItemRegistration.getMobilePhone());
        return "";
    }

    /**
     * mav - Читаем - значение поля Электронная почта <p>("G@merOk.name")
     */
    @Bindable
    public String getEmail() {
        return mItemRegistration.getEmail();
    }

    /**
     * mav - Читаем - значение поля Пароль <p>("password")
     */
    @Bindable
    public String getPassword() {
        return mItemRegistration.getPassword();
    }

    /**
     * mav - Читаем - значение поля UUID динамического ключа<p>("00000000-0000-0000-0000-000000000001")
     */
    @SuppressWarnings("unused")
    @Bindable
    public String getUUID() {
        return mItemRegistration.getUUId();
    }

    /**
     * mav - действия при добавлении симфолов в Фамилию
     */
    public void onSurnameChanged(CharSequence s, int start, int before, int count) {
        mItemRegistration.setSurname("" + s);
        notifyPropertyChanged(BR.surname);
        notifyPropertyChanged(BR.sendColor);
    }

    /**
     * mav - действия при добавлении симфолов в Имя
     */
    public void onFirstNameChanged(CharSequence s, int start, int before, int count) {
        mItemRegistration.setFirstName("" + s);
        notifyPropertyChanged(BR.firstName);
        notifyPropertyChanged(BR.sendColor);
    }

    /**
     * mav - действия при добавлении симфолов в Отчество
     */
    public void onPatronymicChanged(CharSequence s, int start, int before, int count) {
        mItemRegistration.setPatronymic("" + s);
        notifyPropertyChanged(BR.patronymic);
    }

    /**
     * mav - действия при добавлении симфолов в Мобильный телефон
     */
    public void onMobilePhoneChanged(CharSequence s, int start, int before, int count) {
        mItemRegistration.setMobilePhone("" + s);
        notifyPropertyChanged(BR.mobilePhone);
        notifyPropertyChanged(BR.sendColor);
    }

    /**
     * mav - действия при добавлении симфолов в Электронную почту
     */
    public void onEmailChanged(CharSequence s, int start, int before, int count) {
        mItemRegistration.setEmail("" + s);
        notifyPropertyChanged(BR.email);
        notifyPropertyChanged(BR.sendColor);
    }

    /**
     * mav - действия при добавлении симфолов в Электронную почту
     */
    public void onPasswordChanged(CharSequence s, int start, int before, int count) {
        mItemRegistration.setPassword("" + s);
        notifyPropertyChanged(BR.password);
        notifyPropertyChanged(BR.sendColor);
    }

    /**
     * mav - собираем JSON из наших элементов
     */
    @SuppressWarnings("WeakerAccess")
    public String toJSON() {
        // mav 2017.03.01 - генерируем новый ключ
        mItemRegistration.setUUId();

        // mav 2017.03.01 - собираем JSON
        mItemRegistration.setJSON();

        //mav - выводим данные из JSON
        return mItemRegistration.getJSON();
    }

    /**
     * mav - проверяем на корректность ввода наши поля
     */
    @SuppressWarnings("WeakerAccess")
    public String inputValidation() {
        String text = "";
        if (getSurname().trim().length() <= 1)
            text = "Введите фамилию";
        else if (getFirstName().trim().length() <= 1)
            text = "Введите имя";
        else if (getMobilePhone().length() != 10)
            text = "Введите мобильный телефон";
        else if (!getEmail().matches(EMAIL_PATTERN))
            text = "Введите электронную почту";
        else if (getPassword().length() < 7)
            text = "Введите пароль (минимум 7 символов)";
        return text;
    }

    /**
     * mav - Определяем цвет текста кнопки "Отпарвить"
     */
    @Bindable
    public int getSendColor() {
        if (inputValidation().length() > 0)
            return Color.RED;
        return Color.GREEN;
    }


}

package ru.nrgteam.medvdom.honeyagent.global.tools.connector;


import android.content.Context;
import android.util.Log;

import ru.nrgteam.medvdom.honeyagent.global.staticMethods.SF;
import ru.nrgteam.medvdom.honeyagent.global.tools.Con;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;
import ru.nrgteam.medvdom.honeyagent.registration.ItemRegistration;
import ru.nrgteam.medvdom.honeyagent.registration.RegistrationStatic;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.UUID;

import io.realm.Realm;

/**
 * Created by gamerok on 15.03.17.
 * mav - связываемся с сервером тут
 */

public class ServerConnector {
    private static final String TAG = "ServerConnector";

    /**
     * mav - время жизни динамического ключа
     */
    private static final int TIME_RELOAD_DYNAMIC_KEY_MINUTE = 60 * 24 * 3;

    /**
     * mav - время между которым будет проверяться новый ключ
     */
    private static final int TIME_CHECK_DYNAMIC_KEY_MINUTE = 60 * 6;

    /**
     * mav - Таймер на удаление персонального ключа
     */
    private static final int TIME_DELETE_PRIVATE_KEY_MINUTE = 60 * 24 * 14;


    /**
     * mav - Сохраняем в статику персональный ключ
     */
    @SuppressWarnings("WeakerAccess")
    public static volatile String PersonalKey = "";
    /**
     * mav - Сохраняем в статику персональный ключ
     */
    private static volatile String DynamicKey = "";

    /**
     * mav - включаем необходимые обработки
     *
     * @param context контекст
     */
    public ServerConnector(Context context) {

        //mav - проверим что у нас есть персональный ключ
        if ("".equals(PersonalKey)) {
            getPersonalKey(context);
        }

        //mav - проверим что у нас есть динамический ключ
        if ("".equals(DynamicKey)) {
            getDynamicKey(context);
        }
    }


    /**
     * mav - собираем данные для подключения и отдаём результат
     *
     * @param sLink               адрес скрипта
     * @param sContentDisposition дополнительный заголовок для параметров скрипта
     * @param sBody               тело запроса
     * @param ServerType          тип серера (1 - тестовый, 2 - боевой)
     * @return результат запроса
     */
    public static String getResult(String sLink, String sContentDisposition, String sBody, int ServerType) {

        //mav - создаём запись для работы с подключением
        ItemServerConnector itemServerConnector = new ItemServerConnector(sLink, sContentDisposition, sBody, ServerType);

        //mav получаем персональный ключ
        itemServerConnector.setOwner(PersonalKey);

        //mav получаем динамически ключ
        itemServerConnector.setDynamicKey(DynamicKey);

        connectToServer(itemServerConnector);

        return itemServerConnector.getResult();
    }

    /**
     * mav - стартуем саму проверку
     *
     * @param itemServerConnector параметры для запроса
     */
    private static void connectToServer(ItemServerConnector itemServerConnector) {
        connectToServer(1, itemServerConnector);
    }

    /**
     * mav - собственно само выполнение запроса
     *
     * @param countConnections    - номер попытки подключения
     * @param itemServerConnector - сборка данных для подключения
     */
    private static void connectToServer(int countConnections, ItemServerConnector itemServerConnector) {
        String sServerName;

        if (itemServerConnector.getServerType() == 1 && countConnections <= Con.getCountTestServers())
            sServerName = Con.getNextTestServersName(countConnections);
        else if (itemServerConnector.getServerType() == 2 && countConnections <= Con.getCountRootServers())
            sServerName = Con.getNextRootServersName(countConnections);
        else
            return;

        //mav - теперь само подключение
        //mav - строка для получения ответа от сервера
        String s = "";
//
//        Log.i(TAG, "run: " + "Start " + "getDynamicKeyTest (" + countConnections + ", " + sServerName + ")");

        //mav - куда будем отправлять
        String STRING_URL = "http://" + sServerName + "/" + itemServerConnector.getMainAddress() + "/";

        HttpURLConnection httpURLConnection = null;
        try {

            //mav - для начало подготовим URL
            URL url = new URL(STRING_URL);

            //mav - создаём соединение
            httpURLConnection = (HttpURLConnection) url.openConnection();

            //mav - говорим что это POST
            httpURLConnection.setRequestMethod("POST");

            //mav - устанавливаем заголовок для кодировки
            httpURLConnection.setRequestProperty("Content-Type", itemServerConnector.getContentType());
            //mav - устанавливаем заголовок для авторизации
            httpURLConnection.setRequestProperty("Authorization", itemServerConnector.getAuthorization());
            //mav - устанавливаем заголовок для персонального ключа
            httpURLConnection.setRequestProperty("Owner", itemServerConnector.getOwner());
            //mav - устанавливаем заголовок для динамического ключа
            httpURLConnection.setRequestProperty("Dynamic-key", itemServerConnector.getDynamicKey());
            //mav - устанавливаем заголовок для ссылки на внутренний скрипт
            httpURLConnection.setRequestProperty("Link", itemServerConnector.getLink());
            //mav - устанавливаем заголовок для динамического ключа
            httpURLConnection.setRequestProperty("Content-Disposition", itemServerConnector.getContentDisposition());

            //mav - дополнительная заплатка на тот случай, чтобы ответы не путались
            httpURLConnection.setRequestProperty("uuid", UUID.randomUUID().toString());


//                    httpURLConnection.setDoInput(true);
//                    httpURLConnection.setDoOutput(true);
//                    httpURLConnection.setUseCaches(false);

            //mav - нужно указать тело сообщения
            OutputStream outputStream = httpURLConnection.getOutputStream();

            //mav - вставляем нашу фотку или другое тело
            outputStream.write(itemServerConnector.getBody().getBytes());
            outputStream.close();

            //mav - и пробуем отправить
            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());

            //начитываем ответ
            s = SF.readStream(inputStream);
//
//            Log.v(TAG, "connectToServer: " + s);

        } catch (Exception e) {
            // mav 2017.03.16 - думаю это пока можно опустить, вроде насмотрелись на это
            // mav 2017.03.16 - не насмотрелись! смотри ещё!!!
//            Log.w(TAG, "connectToServer: ", e);
        } finally {
            if (httpURLConnection != null) {
                //Закрываем соединение
                httpURLConnection.disconnect();
            }
        }


        //mav - если ответ положительный то пытаемся прикрепить фотку
        if (s.contains("SUCCESSFULLY")) {
            //mav - сохраним наш ответ в нашем классе
            itemServerConnector.setResult(s);

            //noinspection UnnecessaryReturnStatement
            return;
        } else {

            //mav - если там записано значение по умолчанию, то затираем то что было
            if (ItemServerConnector.ERROR_CONNECTOR.equals(itemServerConnector.getResult()))
                itemServerConnector.setResult(itemServerConnector.getLink() + " - " + sServerName + ":" + s);
            else //mav - если там уже был какой-то ответ, то давай его и допишем в наш поток ответов
                itemServerConnector.setResult(itemServerConnector.getResult() + ";\t" + sServerName + ":" + s);

            connectToServer(countConnections + 1, itemServerConnector);
        }
    }

    /**
     * mav - получение персонального ключа из базы данных
     */
    private static void getPersonalKey(Context context) {
        Thread thread = new Thread(() -> {
            try {
                //mav - читаем данные из базы
                //mav - инициализируем (по умолчанию)
                Realm realm = Realm.getInstance(context);

                if (realm.where(ItemPersonalKey.class).count() > 0) {

                    //mav - собственно получение данных из базы данных
                    ItemPersonalKey itemPersonalKey = realm.where(ItemPersonalKey.class).findFirst();

                    PersonalKey = itemPersonalKey.getKey();
                }
                realm.close();

            } catch (Exception e) {
                Log.e(TAG, "checkSending: ", e);

            }
        });
        thread.setPriority(10);  //mav - минимальный приоритет
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * mav - получаем динамический ключ
     */
    public static void getDynamicKey(Context context) {
        Thread thread = new Thread(() -> {
            try {

                //mav - читаем данные из базы
                //mav - инициализируем (по умолчанию)
                Realm realm = Realm.getInstance(context);

                if (realm.where(ItemDynamicKey.class).count() > 0) {

                    //mav - собственно получение данных из базы данных
                    ItemDynamicKey itemDynamicKey = realm.where(ItemDynamicKey.class).findFirst();

                    DynamicKey = itemDynamicKey.getKey();

                    checkDynamicKey(itemDynamicKey, context);
                }
                realm.close();
            } catch (Exception e) {
                Log.e(TAG, "getDynamicKey: ", e);
            }
        });
        thread.setPriority(10);  //mav - максимальный приоритет
//        thread.setDaemon(true); //mav - умри но сделай
        thread.start();
    }

    /**
     * mav - проверяем не пора ли обновить динамический ключ
     */
    private static void checkDynamicKey(ItemDynamicKey itemDynamicKey, Context context) {
        try {


            //mav - проверим на сколько устарел текущий ключ (проверим серверное время)

            boolean isOld = SF.funCheckTimer(TIME_RELOAD_DYNAMIC_KEY_MINUTE, itemDynamicKey.getDateTime());
            boolean isNotChecked = SF.funCheckTimer(TIME_CHECK_DYNAMIC_KEY_MINUTE, itemDynamicKey.getDateConnection());

            if (isOld && isNotChecked) {

                //mav - сохраним дату которая там была прописана, и перезапишемся в Realm, чтобы паралельная проверка не запустилась
                Date date = itemDynamicKey.getDateConnection();

                Realm realm = Realm.getInstance(context);

                realm.beginTransaction();
                itemDynamicKey.setDateConnection();
                realm.commitTransaction();

                //mav - создаём запись для работы с подключением
                ItemServerConnector itemServerConnector = collectDynamicKey();

                //mav - отправляем запрос
                connectToServer(itemServerConnector);

                String s = itemServerConnector.getResult();

                if (s.contains("SUCCESSFULLY")) {
                    JSONObject jsonObject = new JSONObject(s);
                    if ("SUCCESSFULLY".equals(jsonObject.get("scriptResponse"))) {


                        //mav - первым делом обновляем этот динамический ключ
                        DynamicKey = (String) jsonObject.get("Key");

                        realm.beginTransaction();
                        itemDynamicKey.setDateConnection();
                        itemDynamicKey.setKey(DynamicKey);
                        itemDynamicKey.setDateTime(SF.converterFromString((String) jsonObject.get("Time")));
                        realm.commitTransaction();

                        RegistrationStatic.saveDynamicKeyShared(realm, context);

                        Logs.i(TAG, "checkDynamicKey SUCCESSFULLY : " + DynamicKey);
                    }
                } else {
                    //mav - если в этом была проблема, то возвращаем предыдущее время
                    realm.beginTransaction();
                    itemDynamicKey.setDateConnection(date);
                    realm.commitTransaction();

                    //mav - если у нас ничего не получилось то проверим не устарел ли у нас совсем ключ
                    boolean isDeleteTime = SF.funCheckTimer(TIME_DELETE_PRIVATE_KEY_MINUTE, itemDynamicKey.getDateTime());

                    //mav - если совсем устарел - то пришло время отправлять сообщение заново!
                    if (isDeleteTime)
                        deletePersonalKey(context);

                }

                realm.close();
            }


        } catch (Exception e) {
            Log.e(TAG, "checkDynamicKey: ", e);
        }

    }

    /**
     * mav - удаляем персональный ключ, если динамический устарел
     */
    @SuppressWarnings("WeakerAccess")
    public static void deletePersonalKey(Context context) {
        try {
            //mav - сохраняем в базу данных
            //mav - инициализируем (по умолчанию)
            Realm realm = Realm.getInstance(context);

            //mav - если что-то было до этого в базе данных - удаляем и перезаписываем новые данные
            realm.beginTransaction();

            realm.where(ItemRegistration.class).findAll().clear();
            realm.where(ItemDynamicKey.class).findAll().clear();
            realm.where(ItemPersonalKey.class).findAll().clear();

            realm.commitTransaction();
            realm.close();

            //mav - надо удалить динамический ключ из хранилища
            RegistrationStatic.clearRegistration(context);

            System.exit(0);
        } catch (Exception e) {
            Log.e(TAG, "deletePersonalKey: ", e);
        }
    }

    /**
     * mav - собираем данные для запроса динамического ключа
     */
    private static ItemServerConnector collectDynamicKey() {
        //mav - создаём запись для работы с подключением
        ItemServerConnector itemServerConnector = new ItemServerConnector();

        //mav получаем персональный ключ
//        itemServerConnector.setOwner(getPersonalKey(context));
        itemServerConnector.setOwner(PersonalKey);
        //mav получаем динамически ключ
        itemServerConnector.setDynamicKey(DynamicKey);

        itemServerConnector.setLink("genDynamicKey-002/");
        itemServerConnector.setServerType(2);

        return itemServerConnector;
    }

}

package ru.nrgteam.medvdom.honeyagent.global.staticMethods;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import ru.nrgteam.medvdom.honeyagent.logs.Logs;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by gamerok on 22.11.16.
 * перечисляем быстрые и короткие статические методы для работы во всех местах
 */

public class SF {
    private static final String TAG = "SF";

    // Request code for READ_CONTACTS. It can be any number > 0.
    private static final int PERMISSIONS_READ_PHONE_STATE = 100;

    /**
     * mav - преобразуем переменную из NULL в "", если та NULL
     */
    public static String isNull(String string) {
        if (string == null)
            string = "";
        return string;
    }

    /**
     * Начитываем данные и конвертируем их в строку
     *
     * @param in - входные параметры
     * @return - json строку
     */
    @SuppressWarnings("UnusedAssignment")
    public static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder response = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (Exception e) {
            Log.e(TAG, "readStream: ", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    Log.e(TAG, "readStream: ", e);
                }
            }
        }
        return response.toString();
    }

    /**
     * mav - возвращает IMEI телефона
     */
    @SuppressLint("HardwareIds")
    public static String getIMEI(Activity activity) {
        String s = "";
        try {
            TelephonyManager tm = (TelephonyManager) activity
                    .getSystemService(Context.TELEPHONY_SERVICE);
            s = tm.getDeviceId();
        } catch (Exception e) {
            Log.e(TAG, "getIMEI: ", e);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_READ_PHONE_STATE);
            }
        }
        return s;
    }


    /**
     * mav - функция проверяющая прошло ли указанное время или нет
     *
     * @param iPeriodMin       период времени который должен пройти
     * @param flagCalendarTime флаг для текущей проверки (время когда нужно обновить)
     * @return true - если время прошло или наступил следующий день
     */
    public static boolean funCheckTimer(int iPeriodMin, Calendar flagCalendarTime) {
        try {
            //mav - определяем нужно запускать проверку или нет по флагу с коллендарём.
            boolean bFlag;
            //noinspection SimplifiableIfStatement
            if (flagCalendarTime == null)    //mav - если проверка впервые, то считаем что нужно проверять
                bFlag = true;
            else {    //mav - если проверка не в первые, то проверям сколько времени прошло

                //mav - получаем время после
                Calendar calendarTimeForAfter = Calendar.getInstance();
                calendarTimeForAfter.setTime(new Date());
                calendarTimeForAfter.add(Calendar.MINUTE, iPeriodMin);
//                Logs.v(TAG, "funCheckTimer: " + "calendarTimeForAfter = " + calendarTimeForAfter);

                //mav - получаем время перед
                Calendar calendarTimeForBefore = Calendar.getInstance();
                calendarTimeForBefore.setTime(new Date());
                calendarTimeForBefore.add(Calendar.MINUTE, -iPeriodMin);
//                Logs.v(TAG, "funCheckTimer: " + "calendarTimeForBefore = " + calendarTimeForBefore);

                //noinspection RedundantIfStatement
                if (calendarTimeForAfter.after(flagCalendarTime) && calendarTimeForBefore.before(flagCalendarTime))
                    bFlag = false;
                else
                    bFlag = true;

            }

//            if (bFlag)
//                Logs.d(TAG, "funCheckTimer = true");

            return bFlag;
        } catch (Exception e) {
            Log.e(TAG, "funCheckTimer: ", e);
        }
        //mav - если день недели другой, значит точно надо проверять
        return true;
    }

    /**
     * mav - функция проверяющая прошло ли указанное время или нет
     *
     * @param iPeriodMin период времени который должен пройти
     * @return true - если время прошло или наступил следующий день
     */
    public static boolean funCheckTimer(int iPeriodMin, Date date) {
        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return funCheckTimer(iPeriodMin, calendar);
        }else{
            return true;
        }
    }

    /**
     * mav - Засыпаем на несколько миллисекунд
     *
     * @param milliseconds сколько миллисекунд спать
     */
    @SuppressWarnings("unused")
    public static void SleepMilliseconds(int milliseconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException e) {
            Logs.e(TAG, "SleepMilliseconds: ", e);
        }
    }

    /**
     * mav - Засыпаем на несколько секундр
     *
     * @param seconds сколько секунд спать
     */
    public static void SleepSeconds(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            Logs.e(TAG, "SleepMilliseconds: ", e);
        }
    }


    /**
     * mav - при первом старте проверям необходимость миграции
     */
    @SuppressWarnings("CaughtExceptionImmediatelyRethrown")
    public static Realm checkRealm(Context context) {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).build();
        try {
            return Realm.getInstance(realmConfiguration);
        } catch (Exception e) {
            try {
                Realm.deleteRealm(realmConfiguration);
                //Realm file has been deleted.
                return Realm.getInstance(realmConfiguration);
            } catch (Exception ex) {
                throw ex;
                //No Realm file to remove.
            }
        }
    }

    /**
     * mav прячем клавиатуру
     */
    public static void funHideKeyboard(View v, Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            Logs.e(TAG, "funHideKeyboard: ", e);
        }
    }

    /**
     * mav - Конвертируем String в Date
     * @param s String
     * @return Date
     */
    public static Date converterFromString(String s) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS", java.util.Locale.getDefault());
            date = sdf.parse(s);
        } catch (Exception e) {
            Log.e(TAG, "setDateTime: ", e);
        }
        return date;
    }

}

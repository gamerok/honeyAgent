package ru.nrgteam.medvdom.honeyagent.mainService;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import ru.nrgteam.medvdom.honeyagent.logs.Logs;

/**
 * Created by gamerok on 06.06.17.
 * Набор статических переменных и функций для сервиса
 */

public class MainServiceStatic {
    private static final String TAG = "MainServiceStatic";

    public final static String MAIN_SERVICE_HOUR_ALARM = "ru.nrgteam.medvdom.honeyagent.mainService.HourAlarm";

    /**
     * mav - Проверяем включен ли WiFi
     *
     * @param include        true - запускать сервис
     * @param activity     activity
     * @return true - включено, false - выключено
     */
    public static boolean checkServices(boolean include, Activity activity) {
        boolean bStatusAllService;
        try {

            boolean checkMain = checkServiceRunning(MainService.class, activity);
            bStatusAllService = checkMain;
            Logs.v(TAG, "MAIN SERVICE - " + checkMain);
            if (!checkMain) {
                Logs.e(TAG, "MAIN SERVICE DOWN");
                if (include)
                    activity.startService(new Intent(activity, MainService.class));
            }


//            boolean CheckGPS = checkServiceRunning(GPService.class, activity);
//            bCheckAllService = bCheckAllService & CheckGPS;
//            DDLog.v(sCN, sFN, "GPS SERVICE - " + CheckGPS, context);
//            if (!CheckGPS) {
//                DDLog.e(sCN, sFN, "GPS SERVICE DOWN", context);
//                if (isRun)
//                    activity.startService(new Intent(activity, GPService.class));
//            }

//            if (iDealCodeInt > 0) {
//                if (!checkMain)
//                    showMsg("Проблема с сервисом - \"DDService\"!", 5, activity);
//                else if (!checkLOG)
//                    showMsg("Проблема с сервисом - \"DDLog\"!", 5, activity);
//                else if (!checkSQL)
//                    showMsg("Проблема с сервисом - \"DDSQLiteService\"!", 5, activity);
//                else if (!CheckGPS)
//                    showMsg("Проблема с сервисом - \"GPService\"!", 5, activity);
//            }

        } catch (Exception e) {
            bStatusAllService = false;
            Logs.e(TAG, "checkService: ", e);
        }
        return bStatusAllService;
    }

    /**
     * mav - проверим запущен ли сервис
     * @param serviceClass имя сервиса
     * @param context контекст
     * @return true - если запущен
     */
    private static boolean checkServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}

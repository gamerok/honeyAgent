package ru.nrgteam.medvdom.honeyagent.global.tools;

/**
 * Created by gamerok on 23.12.16.
 * mav - класс для хранения всяких штук для подключения к нашим ТЕСТОВЫМ серверам
 */

public class ConT {

    /**
     * mav - Список ТЕСТОВЫХ серверов для подключения
     */
    private static String testServersName[] = {
//            "root_ilc_old.connexus.ru:3080"
//            , "test_ilc_new.connexus.ru:4080"
//            , "root_ilc_old.connexus.ru:5080"
//            , "test_ilc_new.connexus.ru:6080"
//            , "plan_ilc_test.connexus.ru:9080"
//            , "plan_ilc_test.connexus.ru:10080"
            "62.141.96.11:3080"
            , "5.8.177.170:4080"
            , "62.141.96.11:5080"
            , "5.8.177.170:6080"
            , "178.218.119.162:9080"
            , "178.218.119.162:10080"
    };

    /**
     * mav - Список тестовых серверов находящихся в нашей WiFi
     */
    private static String localTestServersName[] = {
            "ILC-SE0001:8080"
            , "ILC-PC21:8080"
            , "ILC-S004:8080" //ILC-S004
            , "ILC-S006:8080" //ILC-S006
    };


    /**
     * mav - номер последнего использованного ТЕСТОВОГО сервера
     */
    private static volatile int CurrentTestServerNumber = 0;
    /**
     * mav - номер последнего использованного ЛОКАЛЬНОГО ТЕСТОВОГО сервера
     */
    private static volatile int CurrentlocalTestServerNumber = 0;

    /**
     * mav - получение следующего по списку имя ТЕСТОВОГО сервера с переключением номера сервера
     */
    @SuppressWarnings("WeakerAccess")
    public static String getNextTestServersName() {
        changeTestServer();
        return testServersName[CurrentTestServerNumber];
    }

    /**
     * mav - получение следующего по списку имя ТЕСТОВОГО сервера с переключением номера сервера
     */
    private static String getLocalNextTestServersName() {
        changeLocalTestServer();
        return localTestServersName[CurrentlocalTestServerNumber];
    }

    /**
     * mav - получение следующего по списку имя ТЕСТОВОГО сервера с переключением номера сервера
     */
    public static String getNextTestServersName(int connectionNumber) {
        if (connectionNumber <= testServersName.length) {
            changeTestServer();
            return testServersName[CurrentTestServerNumber];
        } else {
            return getLocalNextTestServersName();
        }
    }

    /**
     * mav - определяем количество ТЕСТОВЫХ серверов для поиска
     */
    public static int getCountTestServers() {
        return testServersName.length + localTestServersName.length;
    }

    /**
     * mav - меняем ТЕСТОВЫЙ сервер на следующий по порядку
     */
    private static void changeTestServer() {
//        Log.e("TAG", "changeTestServer: " + CurrentTestServerNumber );
        CurrentTestServerNumber = (CurrentTestServerNumber + 1) % testServersName.length;
    }

    /**
     * mav - меняем ЛОКАЛЬНЫЙ ТЕСТОВЫЙ сервер на следующий по порядку
     */
    private static void changeLocalTestServer() {
//        Log.e("TAG", "changeLocalTestServer: " + CurrentTestServerNumber );

        CurrentlocalTestServerNumber = (CurrentlocalTestServerNumber + 1) % localTestServersName.length;
    }


}

package ru.nrgteam.medvdom.honeyagent;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import ru.nrgteam.medvdom.honeyagent.databinding.AMainBinding;
import ru.nrgteam.medvdom.honeyagent.global.staticMethods.SF;
import ru.nrgteam.medvdom.honeyagent.global.tools.connector.PersonalStatus;
import ru.nrgteam.medvdom.honeyagent.global.tools.connector.ServerConnector;
import ru.nrgteam.medvdom.honeyagent.helpMessage.HelpMessage;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;
import ru.nrgteam.medvdom.honeyagent.mainService.MainServiceStatic;
import ru.nrgteam.medvdom.honeyagent.registration.RegistrationApplication;
import ru.nrgteam.medvdom.honeyagent.requests.RequestsActivity;


public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";

    /**
     * mav - сам биндинг
     */
    private AMainBinding mBinding;

    /**
     * mav - письмо в техподдержку
     */
    HelpMessage helpMessage;

    /**
     * mav - переменная календаря чтобы при компиляции он резюме дважды не стартовала и дважды проверка на сервак не отправлялась
     */
    private Calendar calendarOnResume = null;

//    private RegistrationApplication registrationApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.a_main);

        //mav - проверим базу данных на миграцию
        SF.checkRealm(getApplicationContext());

        //mav - неведанная зверюга!
        new Logs(this);

        // mav - это строчка уже присудствует в Логах
//        //mav - коль работаем с подключением, то инициализируем такую штуку
//        new ServerConnector(getApplicationContext());

        //mav - Регистрация
        new RegistrationApplication(mBinding, this);

        //mav - укажем версию приложения
        setVersion();

        //mav - письмо в техподдержку
        helpMessage = new HelpMessage(mBinding, this);

        //mav - инициализируем нажатия на клавные кнопки
        onClickInit();

    }

    /**
     * mav - инициализируем кнопки и их обработку
     */
    private void onClickInit() {
        // mav 2017.03.18 - переход в заявки
        mBinding.btnRequest.setOnClickListener(view -> this.startActivity(new Intent(this, RequestsActivity.class)));

        // mav - звонок в техподдержку
        mBinding.btnHelpCall.setOnClickListener(view -> this.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+78005556327"))));

        //mav - сообщение в техподдержку
        mBinding.btnHelpMessage.setOnClickListener(view -> helpMessage.onClick());

        //mav - тетируем RetroFit
        mBinding.btnShared.setOnClickListener(view -> sharedLink());
    }

    private void sharedLink() {

        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String textToSend="https://play.google.com/store/apps/details?id=ru.nrgteam.medvdom.honeyagent";
        intent.putExtra(Intent.EXTRA_TEXT, textToSend);
        try
        {
            startActivity(Intent.createChooser(intent, "Поделиться ссылкой на приложение"));
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            Toast.makeText(getApplicationContext(), "Some error", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * mav - при обновлении экрана проверяем что у нас там есть в базе данных
     */
    @Override
    protected void onResume() {
        super.onResume();

        //mav - иногда бывает что в этом месте срабатывает сразу два раза, вот сделал небольшую защиту от этого
        if (SF.funCheckTimer(1, calendarOnResume)) {
            //mav - получаем текущее время
            calendarOnResume = Calendar.getInstance();
            calendarOnResume.setTime(new Date());

            //mav - Автоблокировка приложения
            new PersonalStatus(getApplicationContext());

            //mav - статус кнопки
            helpMessage.onResume();

            //mav - проверим на то что есть ли у нас регистрация
            if (ServerConnector.PersonalKey.length() != 0) {

                //mav - если есть то включим сервис
                MainServiceStatic.checkServices(true, this);

                //mav - дополнительно делаем провеку динамического ключа при обновлении формы
                ServerConnector.getDynamicKey(getApplicationContext());
            }

        }

    }

    /**
     * mav - указываем версию на главном экране
     */
    private void setVersion() {
        String s = "v1.00" + getCurrentVersionApp();
        mBinding.tvVersion.setText(s);
    }

    /**
     * mav - получаем текущую версию программы
     *
     * @return версия программы
     */
    public int getCurrentVersionApp() {
        int currentVersion = 0;
        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (Exception e) {
            Logs.e(TAG, "getCurrentVersionApp: ", e);
        }
        return currentVersion;
    }
}

package ru.nrgteam.medvdom.honeyagent.helpMessage;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import ru.nrgteam.medvdom.honeyagent.global.tools.connector.ServerConnector;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;
import ru.nrgteam.medvdom.honeyagent.registration.ItemRegistration;

/**
 * Created by gamerok on 07.04.17.
 * mav - отправка сообщений о помощи
 */

@SuppressWarnings("WeakerAccess")
public class HelpMessageConnection {
    private static final String TAG = "HelpMessageConnection";

    /**
     * mav - текущая версия приложения
     */
    private static int currentVersion = 0;

    /**
     * mav - инициализация собственно класса
     */
    public HelpMessageConnection(Context context){
        if (currentVersion == 0) {
            try {
                currentVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                Logs.e(TAG, "RequestConnection: ", e);
            }
        }

    }

    /**
     * mav - отправка данных по почте
     *
     * @param s текст сообщения
     */
    public static boolean sendToMail(String s, Context context) {
        boolean b = false;
        try {

            //mav - проверим на миграцию базу данных
            Realm realm = Realm.getInstance(context);

            //mav - читаем данные при регистрации
            ItemRegistration itemRegistration = realm.where(ItemRegistration.class).findFirst();

            //mav - собираем текст сообщения
            String sMessageBody = "\nТекст сообщения: \n\n" + s;

            //mav - собираем JSON сообщения
            String sJSON = getJSON(itemRegistration, sMessageBody);


            String sLink = "localSendEmail-004/";
            String sContentDisposition = "";
            String sBody = sJSON;

            // TODO: 07.04.17 Перенести на боевые сервера
            //mav - отправляем запрос
            String sAnswer = ServerConnector.getResult(sLink, sContentDisposition, sBody, 2);

            if (sAnswer.contains("SUCCESSFULLY")) {
                JSONObject jsonObject = new JSONObject(sAnswer);
                if ("SUCCESSFULLY".equals(jsonObject.get("scriptResponse"))
                        && "Sent".equals(jsonObject.get("resultSendingMessage"))) {
                    b = true;
                }
            }

            Log.e(TAG, "sendToMail: " + sAnswer);
        } catch (JSONException e) {
            Logs.e(TAG, "sendToMail: ", e);
        }
        return b;
    }

    /**
     * mav - собираем JSON
     */
    private static String getJSON(ItemRegistration itemRegistration, String sMessageBody) {
        String s;
        JSONObject jsonObject = new JSONObject();
        try {
            String FIO = itemRegistration.getSurname() + " " + itemRegistration.getFirstName() + " " + itemRegistration.getPatronymic();

            jsonObject.put("MessageTo", "app_mail@ilccredits.com");
            jsonObject.put("MessageSubject", "HoneyAgent Письмо в техподдержку от - " + FIO);
            jsonObject.put("MessageBody", sMessageBody + getSubscription(itemRegistration));

            s = jsonObject.toString();
        } catch (Exception e) {
            Log.e(TAG, "getJSONfromParams: ", e);
            s = "";
        }
        return s;
    }

    /**
     * mav - собираем подпись
     */
    private static String getSubscription(ItemRegistration itemRegistration) {
        String s = "";
        try {
            //mav - собираем строку
            s = "\n\n---------\nМобильное приложение (v" + currentVersion + "):"
                    + "\nФамилия: " + itemRegistration.getSurname()
                    + "\nИмя: " + itemRegistration.getFirstName()
                    + "\nОтчество: " + itemRegistration.getPatronymic()
                    + "\nEmail: " + itemRegistration.getEmail()
                    + "\nТелефон: " + itemRegistration.getMobilePhone();

        } catch (Exception e) {
            Logs.e(TAG, "getSubscription: ", e);
        }

        return s;
    }


}

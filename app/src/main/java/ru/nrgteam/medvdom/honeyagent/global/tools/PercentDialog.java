package ru.nrgteam.medvdom.honeyagent.global.tools;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by gamerok on 29.03.17.
 * Диалог с процентами
 */

public class PercentDialog {
    private static final String TAG = "PercentDialog";

    /**
     * mav - Активити. Не понятно что-ли?
     */
    private Activity mActivity;
    /**
     * mav - Диалог с процентами
     */
    private ProgressDialog mDialog;
    /**
     * mav - Диалог с процентами
     */
    private String mText;
    /**
     * mav - Таймер для работы с %
     */
    private Timer mTimerPercent;
    /**
     * mav - определяем текущий шаг прибавления процента
     */
    private int mCount = 0;

    /**
     * mav - Последнее значение процента
     */
    private int mLastPercent;
    /**
     * mav - определяем на сколько мы будем делить проценты
     */
    private int mPart = 0;

    /**
     * mav - Регистрируем обработчик фотографии
     *
     * @param iPart    кол-во частей
     * @param text     текст для диалога
     * @param activity активити
     */
    public PercentDialog(int iPart, String text, Activity activity) {
        mActivity = activity;
        mText = text;
        mPart = iPart;

        //mav - инициализируем необходимые переменные
        init();
    }

    /**
     * mav - инициализируем переменные
     */
    private void init() {
        //mav - диалог
        mDialog = null;
    }

    /**
     * mav - Показываем диалог с процентами
     */
    public void showProgressDialog() {
        try {
            //mav - тут надо ставить проверку есть ли диалог открытый... если есть то сначало попробовать закрыть его... х.з. но на mDialog.show(); иногда вырубает приложение
            closeDialog();

            initProgressDialog();
            mDialog.show();
        } catch (Exception e) {
            Log.e(TAG, "showProgressDialog: ", e);
        }
    }

    /**
     * mav - Закрываем наш диалог
     */
    public void closeDialog() {
        try {
            mActivity.runOnUiThread(() -> {
                if (mDialog != null)
                    if (mDialog.isShowing()) {
                        mDialog.dismiss();
                        mTimerPercent.cancel();
                    }
                mDialog = null;
            });
        } catch (Exception e) {
            Log.e(TAG, "clouseDialog: ", e);
        }
    }

    /**
     * mav - Создаем наш Диалог с процентами
     */
    private void initProgressDialog() {
        try {
            mDialog = new ProgressDialog(mActivity);
            mDialog.setTitle("Подождите.");
            mDialog.setMessage(mText + "0%");
            mLastPercent = 0;
            mCount = 0;
            livePercent();
        } catch (Exception e) {
            Log.e(TAG, "initProgressDialog: ", e);
        }
    }

    /**
     * mav - Таймер для переключения %
     */
    private void livePercent() {
        mTimerPercent = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                mCount = mCount + 1;
                if (mPart > 0 && mCount < mPart && mLastPercent + mCount < 99) {
                    mActivity.runOnUiThread(() -> {
                        if (mDialog != null)
                            if (mDialog.isShowing()) {
                                int i = mLastPercent + mCount;
                                mDialog.setMessage(mText + i + "%");
                            }
                    });
                }
            }
        };

        //mav - первый старнт через 0.1 секунду, следующие попытки через 0.1 секунд
        mTimerPercent.schedule(task, 100, 100);
    }

    /**
     * mav - обновляем цифры на прогресс баре в основном потоке
     *
     * @param percent процент
     */
    @SuppressWarnings("unused")
    public void setPercent(int percent, boolean isFinish) {
        mActivity.runOnUiThread(() -> {
            try {

                if (percent >= 100) {
                    closeDialog();
                    if (isFinish) mActivity.finish();
                } else {
                    if (mDialog != null)
                        if (mDialog.isShowing()) {
                            mLastPercent = percent;
                            mDialog.setMessage(mText + percent + "%");
                            mCount = 0;
                        }
                }
            } catch (Exception e) {
                Log.e(TAG, "funSetPercent: ", e);
            }

        });
    }

}

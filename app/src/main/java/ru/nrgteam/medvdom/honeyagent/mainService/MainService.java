package ru.nrgteam.medvdom.honeyagent.mainService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;

import ru.nrgteam.medvdom.honeyagent.alarms.HourAlarm;
import ru.nrgteam.medvdom.honeyagent.global.tools.connector.ServerConnector;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;

/**
 * Created by gamerok on 02.06.17.
 * Главный сервис. Наконец-то дорос!
 */
public class MainService extends Service {
    private static final String TAG = "MainService";

    /**
     * mav - Состояние сервиса <p>(0 - не стартовал, 1 - onCreate, 2 - onStartCommand)
     */
    public static int ServerStartStatus = 0;

    /**
     * mav - ресивер для обработки сообщений от ежечастного будильника
     */
    private BroadcastReceiver brHourAlarm;

    public MainService() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//
//        //mav - получаем ключи для работы с интернет
//        new ServerConnector(getApplicationContext());

        //maм - приёмник сообщений от будильника
        registerReceiveHourAlarm();

        //mav - мало-ли... чтобы несколько одинаковых будильников не заводить
        if (ServerStartStatus == 0) {
            startAlarms();
        }

        // Говоррим что сервис запущен
        ServerStartStatus = 1;
    }

    @Override
    public void onDestroy() {

        // Говорим что сервис остановлен
        ServerStartStatus = 0;

        if (brHourAlarm != null)
            unregisterReceiver(brHourAlarm);

        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ServerStartStatus == 1) {

            // TODO: 07.06.17 тут буут дазпускаться потоки, которые могли умереть если что-то рошло не так

            //mav - получаем ключи для работы с интернет
            new ServerConnector(getApplicationContext());

            //mav - говорим что сервис полностью запущен
            ServerStartStatus = 2;
        }
        return super.onStartCommand(intent, flags, startId);
    }


    /**
     * Работа с геопозиционированием
     */
    private void startAlarms() {

        try {
            int REQUEST_CODE = 1985;
            Intent intent = new Intent(this, HourAlarm.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, REQUEST_CODE, intent, 0);

            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        int MULTIPLIER = 60 * 60 * 1000; //mav - час
//            int MULTIPLIER = 60 * 1000; //mav - час
            alarmManager.setRepeating(
                    AlarmManager.RTC_WAKEUP, // ориентируемся на системное время
                    System.currentTimeMillis() + MULTIPLIER,
                    MULTIPLIER, // интервал между вызовами
                    pendingIntent);
        } catch (Exception e) {
            Logs.e(TAG, "startAlarms: ", e);
        }
    }

    // Ресивер, который получает ответ от сервиса с координатами
    private void registerReceiveHourAlarm() {
        try {
            brHourAlarm = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    // TODO: 07.06.17 что-то делаем

                    //todo - но эту проверку надо перенести в некий сервис, чтобы эта проверка запускалась например рас в час.
                    //mav - дополнительно делаем провеку динамического ключа при обновлении формы
                    ServerConnector.getDynamicKey(context);

                }
            };
            IntentFilter intFilt = new IntentFilter(MainServiceStatic.MAIN_SERVICE_HOUR_ALARM);
            registerReceiver(brHourAlarm, intFilt);
        } catch (Exception e) {
            Logs.e(TAG, "registerReceiveHourAlarm: ", e);
        }
    }

}

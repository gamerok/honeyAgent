package ru.nrgteam.medvdom.honeyagent.logs;

import android.app.Activity;
import android.util.Log;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import ru.nrgteam.medvdom.honeyagent.global.staticMethods.SF;
import ru.nrgteam.medvdom.honeyagent.global.tools.connector.ServerConnector;

/**
 * Created by gamerok on 21.11.16.
 * mav - класс по сбору логов
 */
public class Logs {
    private static final String TAG = "Logs";

    private static String IMEI = "";

    public Logs(Activity activity){
        if("".equals(Logs.IMEI)){
            IMEI = SF.getIMEI(activity);
        }

        new ServerConnector(activity.getApplicationContext());

    }

    /**
     * mav - Формируем сообщение об ошибке - красный цвет
     *
     * @param sTAG       тэг
     * @param sErrorText текст ошибки
     */
    @SuppressWarnings("unused")
    public static void e(String sTAG, String sErrorText) {
        mainLog("e", sTAG, sErrorText);

        //mav - отправляем на сервер
        sendToServer("e", sTAG, sErrorText, "");
    }

    /**
     * mav - Формируем сообщение об ошибке - красный цвет
     *
     * @param sTAG       тэг
     * @param sErrorText текст ошибки
     */
    @SuppressWarnings("unused")
    public static void wtf(String sTAG, String sErrorText) {
        mainLog("wtf", sTAG, sErrorText);

        //mav - отправляем на сервер
        sendToServer("wtf", sTAG, sErrorText, "");
    }

    /**
     * mav - Формируем сообщение о предупреждении - жёлтый цвет
     *
     * @param sTAG       тэг
     * @param sErrorText текст ошибки
     */
    @SuppressWarnings("unused")
    public static void w(String sTAG, String sErrorText) {
        mainLog("w", sTAG, sErrorText);

        //mav - отправляем на сервер
        sendToServer("w", sTAG, sErrorText, "");
    }

    /**
     * mav - Формируем сообщение о подробностях - синий цвет
     *
     * @param sTAG       тэг
     * @param sErrorText текст ошибки
     */
    @SuppressWarnings("unused")
    public static void v(String sTAG, String sErrorText) {
        mainLog("v", sTAG, sErrorText);

        //mav - отправляем на сервер
        sendToServer("v", sTAG, sErrorText, "");
    }

    /**
     * mav - Формируем сообщение об отладке - белый цвет
     *
     * @param sTAG       тэг
     * @param sErrorText текст ошибки
     */
    @SuppressWarnings("unused")
    public static void d(String sTAG, String sErrorText) {
        mainLog("d", sTAG, sErrorText);

        //mav - отправляем на сервер
        sendToServer("d", sTAG, sErrorText, "");
    }

    /**
     * mav - Формируем сообщение о информации - белый цвет
     *
     * @param sTAG       тэг
     * @param sErrorText текст ошибки
     */
    @SuppressWarnings("unused")
    public static void i(String sTAG, String sErrorText) {
        mainLog("i", sTAG, sErrorText);

        //mav - отправляем на сервер
        sendToServer("i", sTAG, sErrorText, "");
    }

    /**
     * mav - Формируем сообщение о просьбе запустить свою функцию с определённым идентификатором e - красный цвет
     *
     * @param sTAG       тэг
     * @param sErrorText текст ошибки
     * @param e          место ошибки
     */
    public static void e(String sTAG, String sErrorText, Exception e) {
        sErrorText = sErrorText + "\r\n" + funGetStackTrace(e);
        mainLog("e", sTAG, sErrorText);

        //mav - отправляем на сервер
        sendToServer("e", sTAG, sErrorText, funGetStackTrace(e));
    }


    /**
     * mav - Формируем сообщение о просьбе запустить свою функцию с определённым идентификатором w - жёлтый цвет
     *
     * @param sTAG       тэг
     * @param sErrorText текст ошибки
     * @param e          место ошибки
     */
    @SuppressWarnings("unused")
    public static void w(String sTAG, String sErrorText, Exception e) {
        sErrorText = sErrorText + "\r\n" + funGetStackTrace(e);
        mainLog("w", sTAG, sErrorText);

        //mav - отправляем на сервер
        sendToServer("w", sTAG, sErrorText, funGetStackTrace(e));
    }


    /**
     * mav - Данная функция позволяет отправить сообщение с любым тегом
     *
     * @param sType      тип ошибки
     * @param sTAG       тэг
     * @param sErrorText текст ошибки
     */
    private static void mainLog(String sType, String sTAG, String sErrorText) {
        showLog(sType, sTAG, sErrorText);
    }

    /**
     * mav - Функция для вывода логов на первом этапе, далее данные будут приходить к нам на сервер
     *
     * @param sType      тип ошибки
     * @param sTAG       тэг
     * @param sErrorText текст ошибки
     */
    private static void showLog(String sType, String sTAG, String sErrorText) {
        if ("i".equals(sType))
            Log.i(sTAG, sErrorText);
        else if ("v".equals(sType))
            Log.v(sTAG, sErrorText);
        else if ("d".equals(sType))
            Log.d(sTAG, sErrorText);
        else if ("w".equals(sType))
            Log.w(sTAG, sErrorText);
        else if ("e".equals(sType))
            Log.e(sTAG, sErrorText);
        else if ("wtf".equals(sType))
            Log.wtf(sTAG, sErrorText);
    }

    /**
     * mav - Получаем ссылку на место в коде с ошибкой
     *
     * @param error переменная с ошибкой
     */
    private static String funGetStackTrace(Exception error) {
        String s;
        try {
            Writer writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer);
            error.printStackTrace(printWriter);
            s = writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
            error.printStackTrace();
            s = "Error e:" + e.getMessage();
            Log.e(TAG, s);
        }
        s = SF.isNull(s);
        return s;
    }

    /**
     * mav - отправляем на сервак
     */
    private static void sendToServer(String sType, String sTAG, String sErrorText, String errorStackTrace) {

        //mav - выношу время из потока, т.к. потоки могут стартовать как им вздумается
        String sTimestamp_device = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS", java.util.Locale.getDefault()).format(new Date());

        Thread thread = new Thread(() -> {
            try {
                String sLink = "bestCredits-004/fn_local_save_app_logs_002";
                String sContentDisposition = "duration_cache=\"0\".";
                String sBody = getJSONfromParams(sType, sTAG, sErrorText, errorStackTrace, sTimestamp_device);

                // TODO: 07.04.17 Перенести на боевые сервера
                //mav - отправляем запрос
                @SuppressWarnings("unused")
                String sAnswer = ServerConnector.getResult(sLink, sContentDisposition, sBody, 2);

            } catch (Exception e) {
                Log.e(TAG, "checkSending: ", e);

            }
        });
        thread.setPriority(1);  //mav - минимальный приоритет
        thread.start();

    }

    /**
     * mav - собираем JSON
     */
    private static String getJSONfromParams(String sType, String sTAG, String sErrorText, String errorStackTrace, String sTimestamp_device) {
        String s;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("device_id", IMEI + "h");
            jsonObject.put("timestamp_device", sTimestamp_device);
            jsonObject.put("prefix_message", sType);
            jsonObject.put("tag_message", sTAG);
            jsonObject.put("text_message", sErrorText);
            jsonObject.put("stack_trace", errorStackTrace);

            s = jsonObject.toString();
        } catch (Exception e) {
            Log.e(TAG, "getJSONfromParams: ", e);
            s = "";
        }
        return s;
    }


}

package ru.nrgteam.medvdom.honeyagent.helpMessage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import ru.nrgteam.medvdom.honeyagent.R;
import ru.nrgteam.medvdom.honeyagent.databinding.AMainBinding;
import ru.nrgteam.medvdom.honeyagent.global.staticMethods.SF;
import ru.nrgteam.medvdom.honeyagent.global.tools.PercentDialog;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;

/**
 * Created by gamerok on 30.03.17.
 * обработчик нажатия на кнопку помощи
 */

public class HelpMessage {
    private static final String TAG = "HelpMessage";

    /**
     * mav - активити
     */
    private Activity mActivity;
    /**
     * mav - контекст
     */
    private Context mContext;
    /**
     * mav - сам биндинг
     */
    private AMainBinding mBinding;

    /**
     * mav - Доступ
     */
    private SharedPreferences sharedPreferences;

    /**
     * mav - Ключик доя доступак определённым внутренним хранилищам
     */
    private static final String SEND_HELP_PREFERENCES = "SendHelp_sharedPreferences";
    /**
     * mav - Ключик доя доступак определённым внутренним хранилищам
     */
    private static final String SEND_HELP_PREFERENCES_TIMER_RECORD = "SendHelp_sharedPreferences_TimerRecord";

    /**
     * mav - параметр устанавливающий промежутки между отправками сообщений
     */
    private static final int SEND_HELP_INTERVAL_MIN = 60;

    /**
     * mav - флаг обозначающий текущее состояние кнопки - активна или нет.
     */
    private boolean bStatusIsActive = true;
    /**
     * mav - диалоговое окно с процентами
     */
    private PercentDialog mPercentDialog;

    /**
     * mav - инициализируем всю обработку
     */
    public HelpMessage(AMainBinding binding, Activity activity) {
        try {
            mActivity = activity;
            mContext = activity.getApplicationContext();
            mBinding = binding;

            //mav - получаем доступ к внутреннему хранилищу
            sharedPreferences = mContext.getSharedPreferences(SEND_HELP_PREFERENCES, Context.MODE_PRIVATE);

            //mav - будем отправлять наши сообщения
            new HelpMessageConnection(mContext);

            //mav - инициализация диалогового окна с процентами
            mPercentDialog = new PercentDialog(100, "Отправляю сообщение...", activity);
        } catch (Exception e) {
            Logs.e(TAG, "HelpMessage: ", e);
        }
    }

    /**
     * mav - при обновлении формы нужно проверить в какой цвет следует подкрашивать кноку... мол если нажали один раз то второй раз можно нажать только через час
     */
    public void onResume() {
        try {
            //mav - проверим в какой цвет выкрашивать кнопку
            if (SF.funCheckTimer(SEND_HELP_INTERVAL_MIN, getHelpMessageTimeCalendar())) {
                //mav - если True - значит время прошло и можно подкрашивать кноку в активный цвет, иначе в пассивный цвет
                mBinding.btnHelpMessage.setTextColor(Color.WHITE);
                bStatusIsActive = true;
            } else {
                //mav - иначе перекрашиваем кнопку в серый цвет
                mBinding.btnHelpMessage.setTextColor(Color.GRAY);
                bStatusIsActive = false;
            }
        } catch (Exception e) {
            Logs.e(TAG, "onResume: ", e);
        }
    }

    /**
     * mav - список действий при нажатии на кнопку Письмо в техподдержку
     */
    public void onClick() {
        try {
            if (bStatusIsActive) {
                //mav - билдер для создания диалога
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.MyDialogTheme);
                // Get the layout inflater
                LayoutInflater inflater = mActivity.getLayoutInflater();

                //mav - получаем корневую Вью, для обращения к полю ввода
                @SuppressLint("InflateParams")
                View view = inflater.inflate(R.layout.dialog_help_messages, null);

                //mav - поле для ввода
                EditText editText = (EditText) view.findViewById(R.id.et_dialog_help_messages);

                //mav - собираем действия и кнопки
                builder.setView(view)
                        .setPositiveButton(R.string.all_send, (dialogInterface, i) -> sendMessage(editText.getText().toString()))
                        .setNegativeButton(R.string.all_close, null);

                //mav - передаём всё это многообразие в AlertDialog
                AlertDialog alertDialog;
                alertDialog = builder.create();

                //mav - отображаем клавиатуру
                //noinspection ConstantConditions
                alertDialog.getWindow()
                        .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

                //mav - отображение диалога
                alertDialog.show();
            } else {
                String s = "Интервал между отправками сообщений в техподдержку составляет 60 минут.";

                //mav - диалоговое окно перед отправкой
                new AlertDialog.Builder(
                        mActivity)
                        .setTitle("Обратите внимание!")
                        .setMessage(s)
                        .setCancelable(false) //mav - чтобы сообщение не закрывалось при клике на свободное место
                        .setNegativeButton(R.string.all_close, null)
                        .create()
                        .show();

                //mav - перекрасим цвет текста в нужный цвет
                onResume();
            }
        } catch (Exception e) {
            Logs.e(TAG, "onClick: ", e);
        }
    }

    /**
     * mav - отправляем сообщение
     *
     * @param s текст сообщения
     */
    private void sendMessage(String s) {
        //mav - показываем диалог
        mPercentDialog.showProgressDialog();

        Thread thread = new Thread(() -> sendMessageInThread(s));
        thread.setPriority(10);  //mav - минимальный приоритет
//        thread.setDaemon(true);   //mav - умри но отправь!
        thread.start();
    }

    /**
     * mav - отпавка сообщения в потоке
     *
     * @param s текст сообщения
     */
    private void sendMessageInThread(String s) {
        try {

            boolean bStatus = HelpMessageConnection.sendToMail(s, mContext);

            //mav - отображаем пользователю отправили мы сообщение или нет
            mActivity.runOnUiThread(() -> showStatusSend(bStatus));

        } catch (Exception e) {
            Logs.e(TAG, "sendMessageInThread: ", e);
        }
        //mav - закрываем диалог
        mPercentDialog.closeDialog();
    }

    /**
     * mav - отображаем пользователю результат отправки сообщения
     *
     * @param bStatus true - отправлено
     */
    private void showStatusSend(boolean bStatus) {
        String s;

        //mav - собираем текст сообщения
        if (bStatus) {
            s = "Сообщение успешно отправлено!";

            //mav - Успешно отправили сообщение, кнопку можно прятать
            sendSuccessMessage();
        }else
            s = "ОШИБКА! Сообщение не отправлено!";


        //mav - отображаем сообщение
        Toast.makeText(mActivity, s, Toast.LENGTH_LONG).show();
    }

    /**
     * mav - если сообщение отправили успешно
     */
    private void sendSuccessMessage() {
        //mav - сохраняем текущее время нажатия на кнопку
        setHelpMessageTime();

        //mav - перекрасим цвет текста в нужный цвет
        onResume();
    }


    /**
     * mav - сохраняем данные в внутренее хранилище
     *
     * @param l - значение для сохранения
     */
    private void setHelpMessageTime(long l) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(SEND_HELP_PREFERENCES_TIMER_RECORD, l);
            editor.apply();
        } catch (Exception e) {
            Logs.e(TAG, "setHelpMessageTime: ", e);
        }
    }

    /**
     * mav - сохраняем данные в внутренее хранилище
     *
     * @param calendar - значение для сохранения
     */
    private void setHelpMessageTime(Calendar calendar) {
        try {
            long l = calendar.getTimeInMillis();
            setHelpMessageTime(l);
        } catch (Exception e) {
            Logs.e(TAG, "setHelpMessageTime: ", e);
        }
    }

    /**
     * mav - сохраняем данные в внутренее хранилище с текущим временем
     */
    private void setHelpMessageTime() {
        try {
            //mav - получаем текущее время
            Calendar calendarTime = Calendar.getInstance();
            calendarTime.setTime(new Date());
            setHelpMessageTime(calendarTime);
        } catch (Exception e) {
            Logs.e(TAG, "setHelpMessageTime: ", e);
        }
    }

    /**
     * mav - читаем время из хранилища
     *
     * @return кол-во миллисекунд
     */
    private long getHelpMessageTimeLong() {
        long l = 0;
        try {
            l = sharedPreferences.getLong(SEND_HELP_PREFERENCES_TIMER_RECORD, 0);
        } catch (Exception e) {
            Logs.e(TAG, "getHelpMessageTimeLong: ", e);
        }
        return l;
    }

    /**
     * mav - читаем время из хранилища
     */
    private Calendar getHelpMessageTimeCalendar() {
        Calendar calendar = null;
        try {
            long l = getHelpMessageTimeLong();

            calendar = Calendar.getInstance();
            calendar.setTimeInMillis(l);

        } catch (Exception e) {
            Logs.e(TAG, "getHelpMessageTimeCalendar: ", e);
        }
        return calendar;
    }


}

package ru.nrgteam.medvdom.honeyagent.requests;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ru.nrgteam.medvdom.honeyagent.R;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;

import java.util.List;

/**
 * Created by gamerok on 20.03.17.
 * Адаптер для заполнения списка введённых заявок
 */

@SuppressWarnings("WeakerAccess")
public class RequestAdapter extends BaseAdapter {
    private static final String TAG = "RequestAdapter";

    /**
     * mav - список заявок
     */
    private List<ItemRequest> mL_ItemRequest;

    /**
     * mav - автоматически пытается присоединить надутые компоненты к корневому элементу
     */
    private LayoutInflater lInflater;

    public RequestAdapter(List<ItemRequest> itemRequests, Context context) {
        mL_ItemRequest = itemRequests;
        lInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mL_ItemRequest.size();
    }

    @Override
    public Object getItem(int i) {
        return mL_ItemRequest.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    /**
     * mav - класс с полями
     */
    private static class ViewHolder {
        TextView tvId;
        TextView tvSurname;
        TextView tvMobilePhone;
        ImageView iv_StatusSend;
        TextView tvStatus;
    }

    /**
     * mav - определяем значёк в зависимости от статуса отправки
     */
    private int getImageId(int iStatus) {
        if (iStatus == 2)
            return R.drawable.green_confirmation;
        else
            return R.drawable.yellow_confirmation;
    }

    /**
     * mav - определяем статус заявки
     */
    private String getStatus(int iStatus) {
        if (iStatus == 1)
            return "Создана";
        return "Создана";

    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            //mav - собираем список элементов
            ViewHolder viewHolder;

            //mav - определяем брать элемент из стека или создать новый, чтобы работало быстрее
            if (view == null) {
                view = lInflater.inflate(R.layout.item_request, null);
                viewHolder = new ViewHolder();

                viewHolder.tvId = (TextView) view.findViewById(R.id.tv_id);
                viewHolder.tvSurname = (TextView) view.findViewById(R.id.tv_FIO);
                viewHolder.tvMobilePhone = (TextView) view.findViewById(R.id.tv_MobilePhone);
                viewHolder.iv_StatusSend = (ImageView) view.findViewById(R.id.iv_StatusSend);
                viewHolder.tvStatus = (TextView) view.findViewById(R.id.tv_Status);

                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }



            //mav - заполняем наши поля
            //mav - номер записи в базе данных
            int iId = mL_ItemRequest.size() - i;
            String sId = "" + iId;
            viewHolder.tvId.setText(sId);

            //mav - фамилия имя отчество
            viewHolder.tvSurname.setText(mL_ItemRequest.get(i).getSurname() + " " + mL_ItemRequest.get(i).getFirstName() + " " + mL_ItemRequest.get(i).getPatronymic());

            //mav - мобильный телефон
            String sMobilePhone = "" + mL_ItemRequest.get(i).getMobilePhone();
            sMobilePhone = "*" + sMobilePhone.substring(7, sMobilePhone.length());

            viewHolder.tvMobilePhone.setText(sMobilePhone);

            //mav - Статус
            viewHolder.tvStatus.setText(getStatus(mL_ItemRequest.get(i).getStatus()));

            //mav - статус отправки
            viewHolder.iv_StatusSend.setImageResource(getImageId(mL_ItemRequest.get(i).getIsSend()));

            //mav - покрасим строчки
            setColor(i, viewHolder);

        } catch (Exception e) {
            Logs.e(TAG, "getView: ", e);
        }
        return view;
    }

    /**
     * mav - подкрашиваем строчки с мёдом
     * @param i тип заявки
     * @param viewHolder класс где хранятся все элементы
     */
    private void setColor(int i, ViewHolder viewHolder){
        //mav - если заказали мёд, надо подкрасить строку в жёлтый цвет
        if(mL_ItemRequest.get(i).getType()==1){
            viewHolder.tvSurname.setTextColor(Color.YELLOW);
            viewHolder.tvMobilePhone.setTextColor(Color.YELLOW);
            viewHolder.tvStatus.setTextColor(Color.YELLOW);
        }else{
            viewHolder.tvSurname.setTextColor(Color.WHITE);
            viewHolder.tvMobilePhone.setTextColor(Color.WHITE);
            viewHolder.tvStatus.setTextColor(Color.WHITE);
        }
    }

}


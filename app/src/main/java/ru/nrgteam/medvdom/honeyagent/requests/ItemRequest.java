package ru.nrgteam.medvdom.honeyagent.requests;

import android.util.Log;

import ru.nrgteam.medvdom.honeyagent.logs.Logs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by gamerok on 18.03.17.
 * Записи для работы с заявками
 */

@SuppressWarnings("WeakerAccess")
public class ItemRequest extends RealmObject {
    private static final String TAG = "ItemRequest";

    /**
     * mav - UUId для динамического ключа <p>("00000000-0000-0000-0000-000000000001")
     */
    @PrimaryKey
    private String UUId;
    /**
     * mav - Фамилия <p>("Мятечкин")
     */
    private String Surname;
    /**
     * mav - Имя <p>("Александр")
     */
    private String FirstName;
    /**
     * mav - Отчество <p>("Викторович")
     */
    private String Patronymic;
    /**
     * mav - Мобильный телефон <p>("9103089229")
     */
    private long MobilePhone;
    /**
     * mav - Тип заявки <p>(1 - мёд, 2 - деньги)
     */
    private int Type;
    /**
     * mav - Время создания записи на мобильном устройстве <p>("2017-02-17 16:59:52.17117")
     */
    private Date DateTime;
    /**
     * mav - Статус текущей заявки <p>(1 - создана на мобильном телефоне)
     */
    private int Status;
    /**
     * mav - Статус отправки данных <p>(1 - не отправлено на сервер. 2 - отправлено на сервер, 3 - отправляется)
     */
    private int IsSend;

    /**
     * mav - инициализация класса
     */
    public ItemRequest() {
        Surname = "";
        FirstName = "";
        Patronymic = "";
        MobilePhone = -1;
        UUId = "00000000-0000-0000-0000-000000000001";
        Type = 2;
        DateTime = null;
        Status = 0;
        IsSend = 0;
    }


    /**
     * mav - Устанавливаем - значение поля Фамилия <p>("Мятечкин")
     */
    public void setSurname(String surname) {
        if (surname != null && surname.length() >= 0) {
            this.Surname = surname.replaceAll("\"", "").replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("'", "");
//            Log.e(TAG, "setSurname: "+surname );
        }
    }

    /**
     * mav - Читаем - значение поля Фамилия <p>("Мятечкин")
     */
    public String getSurname() {
        return Surname;
    }


    /**
     * mav - Устанавливаем - значение поля Имя <p>("Александрп")
     */
    public void setFirstName(String firstName) {
        if (firstName != null && firstName.length() >= 0) {
            FirstName = firstName.replaceAll("\"", "").replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("'", "");
//            Log.e(TAG, "setFirstName: " + FirstName);
        }
    }

    /**
     * mav - Читаем - значение поля Имя <p>("Александрп")
     */
    public String getFirstName() {
        return FirstName;
    }


    /**
     * mav - Устанавливаем - значение поля Отчество <p>("Викторович")
     */
    public void setPatronymic(String patronymic) {
        if (patronymic != null && patronymic.length() >= 0) {
            Patronymic = patronymic.replaceAll("\"", "").replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("'", "");
//            Log.e(TAG, "setPatronymic: " + Patronymic);
        }
    }

    /**
     * mav - Читаем - значение поля Отчество <p>("Викторович")
     */
    public String getPatronymic() {
        return Patronymic;
    }


    /**
     * mav - Устанавливаем - значение поля Мобильный телефон <p>("9103089229")
     */
    public void setMobilePhone(long mobilePhone) {
        if (mobilePhone >= 0) {
            MobilePhone = mobilePhone;
//            Log.e(TAG, "setMobilePhone: " + MobilePhone);
        }
    }

    /**
     * mav - Устанавливаем - значение поля Мобильный телефон <p>("9103089229")
     */
    @SuppressWarnings("WeakerAccess")
    public void setMobilePhone(String mobilePhone) {
        try {
            if (mobilePhone.length() == 0)
                setMobilePhone(0);
            else {
                long l = Long.valueOf(mobilePhone);
                setMobilePhone(l);
            }
        } catch (Exception e) {
            Logs.e(TAG, "setMobilePhone: ", e);
        }
    }

    /**
     * mav - Читаем - значение поля Мобильный телефон <p>("9103089229")
     */
    public long getMobilePhone() {
        return MobilePhone;
    }

    /**
     * mav - Устанавливаем - значение поля UUId динамического ключа<p>("00000000-0000-0000-0000-000000000001")
     */
    public void setUUId(String UUId) {
        if (UUId != null && UUId.length() == 36) {
            this.UUId = UUId;
        }
    }

    /**
     * mav - Генерируем - значение поля UUId динамического ключа<p>("00000000-0000-0000-0000-000000000001")
     */
    @SuppressWarnings("WeakerAccess")
    public void setUUId() {
        setUUId(UUID.randomUUID().toString());
    }

    /**
     * mav - Читаем - значение поля UUId динамического ключа<p>("00000000-0000-0000-0000-000000000001")
     */
    public String getUUId() {
        return UUId;
    }


    /**
     * mav - Тип заявки <p>(1 - мёд, 2 - деньги)
     */
    public void setType(int type) {
        if (type > 0)
            Type = type;
    }

    /**
     * mav - Тип заявки <p>(1 - мёд, 2 - деньги)
     */
    public int getType() {
        return Type;
    }


    /**
     * mav - Устанавливаем - Время создания записи на мобильном устройстве <p>("2017-02-17 16:59:52.17117")
     */
    public void setDateTime() {
        setDateTime(new Date());
    }

//    /**
//     * mav - Устанавливаем - Время создания записи на мобильном устройстве <p>("2017-02-17 16:59:52.17117")
//     */
//    public void setDateTime(String dateTime) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS", java.util.Locale.getDefault());
//        try {
//            Date convertedDate = sdf.parse(dateTime);
//            setDateTime(convertedDate);
//        } catch (Exception e) {
//            Log.e(TAG, "setDateTime: ", e);
//        }
//    }

    /**
     * mav - Устанавливаем - Время создания записи на мобильном устройстве <p>("2017-02-17 16:59:52.17117")
     */
    public void setDateTime(Date dateTime) {
        DateTime = dateTime;
    }


    /**
     * mav - Читаем - Время создания записи на мобильном устройстве <p>("2017-02-17 16:59:52.17117")
     */
    public Date getDateTime() {
        return DateTime;
    }


    /**
     * mav - Устанавливаем - Статус текущей заявки <p>(1 - создана на мобильном телефоне)
     */
    public void setStatus(int status) {
        Status = status;
    }

    /**
     * mav - Читаем - Статус текущей заявки <p>(1 - создана на мобильном телефоне)
     */
    public int getStatus() {
        return Status;
    }


    /**
     * mav - Статус отправки данных <p>(1 - не отправлено на сервер. 2 - отправлено на сервер)
     */
    public void setIsSend(boolean isSend) {
        setIsSend(isSend ? 2 : 1);
    }

    /**
     * mav - Статус отправки данных <p>(1 - не отправлено на сервер. 2 - отправлено на сервер, 3 - отправляется)
     */
    public void setIsSend(int isSend) {
        if (isSend > 0)
            IsSend = isSend;
    }

    /**
     * mav - Статус отправки данных <p>(1 - не отправлено на сервер. 2 - отправлено на сервер, 3 - отправляется)
     */
    public int getIsSend() {
        return IsSend;
    }
}


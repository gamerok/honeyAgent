package ru.nrgteam.medvdom.honeyagent.registration;


import ru.nrgteam.medvdom.honeyagent.logs.Logs;

import org.json.JSONObject;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by gamerok on 22.02.17.
 * класс для хранения переменных
 */

public class ItemRegistration extends RealmObject {
    private static final String TAG = "ItemRegistration";

//    private static final String UUID_NULL = "00000000-0000-0000-0000-000000000001";

    /**
     * mav - Фамилия <p>("Мятечкин")
     */
    @PrimaryKey
    private String Surname;

    /**
     * mav - Имя <p>("Александр")
     */
    private String FirstName;

    /**
     * mav - Отчество <p>("Викторович")
     */
    private String Patronymic;

    /**
     * mav - Мобильный телефон <p>("9103089229")
     */
    private long MobilePhone;

    /**
     * mav - Электронная почта <p>("G@merOk.name")
     */
    private String Email;

    /**
     * mav - Пароль <p>("password")
     */
    private String Password;

    /**
     * mav - UUId для динамического ключа <p>("00000000-0000-0000-0000-000000000001")
     */
    private String UUId;

    /**
     * mav - JSON для отправки <p>
     * ("{"Surname":"ТестоваяФамилия"
     * <p>,"FirstName":"ТестовоеИмя"
     * <p>,"Patronymic":"Отчество"
     * <p>,"MobilePhone":"9876543210"
     * <p>,"Email":"g@merok.name"
     * <p>,"Password":"gjnvvccvhk"
     * <p>,"UUId":"63e6f1b5-b53c-494a-904a-e8fc1fe1eb0a"}")
     */
    private String JSON;

    public ItemRegistration() {
        Surname = "";
        FirstName = "";
        Patronymic = "";
        MobilePhone = -1;
        Email = "";
        Password = "";
        UUId = "00000000-0000-0000-0000-000000000001";
        JSON = "";
    }


    /**
     * mav - Устанавливаем - значение поля Фамилия <p>("Мятечкин")
     */
    public void setSurname(String surname) {
        if (surname != null && surname.length() >= 0) {
            this.Surname = surname.replaceAll("\"", "").replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("'", "");
//            Log.e(TAG, "setSurname: "+surname );
        }
    }

    /**
     * mav - Читаем - значение поля Фамилия <p>("Мятечкин")
     */
    public String getSurname() {
        return Surname;
    }


    /**
     * mav - Устанавливаем - значение поля Имя <p>("Александрп")
     */
    public void setFirstName(String firstName) {
        if (firstName != null && firstName.length() >= 0) {
            FirstName = firstName.replaceAll("\"", "").replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("'", "");
//            Log.e(TAG, "setFirstName: " + FirstName);
        }
    }

    /**
     * mav - Читаем - значение поля Имя <p>("Александрп")
     */
    public String getFirstName() {
        return FirstName;
    }


    /**
     * mav - Устанавливаем - значение поля Отчество <p>("Викторович")
     */
    public void setPatronymic(String patronymic) {
        if (patronymic != null && patronymic.length() >= 0) {
            Patronymic = patronymic.replaceAll("\"", "").replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("'", "");
//            Log.e(TAG, "setPatronymic: " + Patronymic);
        }
    }

    /**
     * mav - Читаем - значение поля Отчество <p>("Викторович")
     */
    public String getPatronymic() {
        return Patronymic;
    }


    /**
     * mav - Устанавливаем - значение поля Мобильный телефон <p>("9103089229")
     */
    public void setMobilePhone(long mobilePhone) {
        if (mobilePhone >= 0) {
            MobilePhone = mobilePhone;
//            Log.e(TAG, "setMobilePhone: " + MobilePhone);
        }
    }

    /**
     * mav - Устанавливаем - значение поля Мобильный телефон <p>("9103089229")
     */
    @SuppressWarnings("WeakerAccess")
    public void setMobilePhone(String mobilePhone) {
        try {
            if (mobilePhone.length() == 0)
                setMobilePhone(0);
            else {
                long l = Long.valueOf(mobilePhone);
                setMobilePhone(l);
            }
        } catch (Exception e) {
            Logs.e(TAG, "setMobilePhone: ", e);
        }
    }

    /**
     * mav - Читаем - значение поля Мобильный телефон <p>("9103089229")
     */
    public long getMobilePhone() {
        return MobilePhone;
    }


    /**
     * mav - Устанавливаем - значение поля Электронная почта <p>("G@merOk.name")
     */
    public void setEmail(String email) {
        if (email != null && email.length() >= 0) {
            Email = email.trim().replaceAll("\"", "").replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("'", "");
//            Log.e(TAG, "setEmail: " + Email);
        }
    }

    /**
     * mav - Читаем - значение поля Электронная почта <p>("G@merOk.name")
     */
    public String getEmail() {
        return Email;
    }


    /**
     * mav - Устанавливаем - значение поля Пароль <p>("password")
     */
    public void setPassword(String password) {
        if (password != null && password.length() >= 0) {
            Password = password.trim().replaceAll("\"", "").replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("'", "");
//            Log.e(TAG, "setPassword: " + Password);
        }
    }

    /**
     * mav - Читаем - значение поля Пароль <p>("password")
     */
    public String getPassword() {
        return Password;
    }

    /**
     * mav - Устанавливаем - значение поля UUId динамического ключа<p>("00000000-0000-0000-0000-000000000001")
     */
    public void setUUId(String UUId) {
        if (UUId != null && UUId.length() == 36) {
            this.UUId = UUId;
//            Logs.e(TAG, "setUUId: " + UUId);

//            this.UUId = "485a7aa0-e098-4268-9028-410daa459ba6";

        }
    }

    /**
     * mav - Генерируем - значение поля UUId динамического ключа<p>("00000000-0000-0000-0000-000000000001")
     */
    @SuppressWarnings("WeakerAccess")
    public void setUUId() {
//        if (UUID_NULL.equals(UUId))
        setUUId(UUID.randomUUID().toString());
    }

    /**
     * mav - Читаем - значение поля UUId динамического ключа<p>("00000000-0000-0000-0000-000000000001")
     */
    public String getUUId() {
        return UUId;
    }


    /**
     * mav - Устанавливаем - значение поля JSON для отправки <p>
     * ("{"Surname":"ТестоваяФамилия"
     * <p>,"FirstName":"ТестовоеИмя"
     * <p>,"Patronymic":"Отчество"
     * <p>,"MobilePhone":"9876543210"
     * <p>,"Email":"g@merok.name"
     * <p>,"Password":"gjnvvccvhk"
     * <p>,"UUId":"63e6f1b5-b53c-494a-904a-e8fc1fe1eb0a"}")
     */
    public void setJSON(String JSON) {
        if (JSON != null && JSON.length() > 36) {
            this.JSON = JSON;
        }
    }

    /**
     * mav - Генерируем - значение поля JSON для отправки <p>
     * ("{"Surname":"ТестоваяФамилия"
     * <p>,"FirstName":"ТестовоеИмя"
     * <p>,"Patronymic":"Отчество"
     * <p>,"MobilePhone":"9876543210"
     * <p>,"Email":"g@merok.name"
     * <p>,"Password":"gjnvvccvhk"
     * <p>,"UUId":"63e6f1b5-b53c-494a-904a-e8fc1fe1eb0a"}")
     */
    @SuppressWarnings("WeakerAccess")
    public void setJSON() {
        String s;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("surname", getSurname().trim());
            jsonObject.put("firstName", getFirstName().trim());
            jsonObject.put("patronymic", getPatronymic().trim());
            jsonObject.put("mobilePhone", "7" + getMobilePhone());
            jsonObject.put("email", getEmail());
            jsonObject.put("password", getPassword());
            jsonObject.put("UUId", getUUId());

            s = jsonObject.toString();
        } catch (Exception e) {
            Logs.e(TAG, "setJSON: ", e);
            s = "";
        }
        setJSON(s);
    }


    /**
     * mav - Читаем - значение поля JSON для отправки <p>
     * ("{"Surname":"ТестоваяФамилия"
     * <p>,"FirstName":"ТестовоеИмя"
     * <p>,"Patronymic":"Отчество"
     * <p>,"MobilePhone":"9876543210"
     * <p>,"Email":"g@merok.name"
     * <p>,"Password":"gjnvvccvhk"
     * <p>,"UUId":"63e6f1b5-b53c-494a-904a-e8fc1fe1eb0a"}")
     */
    public String getJSON() {
        return JSON;
    }
}

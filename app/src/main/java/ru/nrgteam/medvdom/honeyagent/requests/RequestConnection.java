package ru.nrgteam.medvdom.honeyagent.requests;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import ru.nrgteam.medvdom.honeyagent.global.tools.connector.ServerConnector;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;
import ru.nrgteam.medvdom.honeyagent.registration.ItemRegistration;

/**
 * Created by gamerok on 22.03.17.
 * Класс для отправки данных
 */

@SuppressWarnings("WeakerAccess")
public class RequestConnection {
    private static final String TAG = "RequestConnection";

    /**
     * mav - защита от одновременного старта
     */
    private static volatile boolean isStart = false;

    /**
     * mav - текущая версия приложения
     */
    private static int currentVersion = 0;


    public RequestConnection(Context context) {
        isStart = false;
        if (currentVersion == 0) {
            try {
                currentVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                Logs.e(TAG, "RequestConnection: ", e);
            }
        }
    }


    //mav - проверяем есть ли что отправлять
    public static long checkSending(int iIsSend, Context context) {
        //mav - сохраняем в базу данных
        //mav - проверим на миграцию базу данных
        Realm realm = Realm.getInstance(context);
        //mav - если что-то было до этого в базе данных - удаляем и перезаписываем новые данные
        long l = realm.where(ItemRequest.class).equalTo("IsSend", iIsSend).count();
        realm.close();

        return l;
    }

    /**
     * mav - отправка данных
     *
     * @param lvRequest лист заявок
     * @param activity  активити
     */
    public static void sendData(ListView lvRequest, Activity activity) {
        if (isStart)
            return;

        //mav - защита от нескольких стартов
        isStart = true;

        Thread thread = new Thread(() -> {
            try {
                //mav - сохраняем в базу данных

                // mav 2017.03.22 - находим сколько там записей у нас ожидают отправки
                long countItem = checkSending(1, activity.getApplicationContext());

                //mav - если в базе нечего отправлять - то просто выходим отсюда
                if (countItem > 0) {

                    //mav - класс в который будем начитывать данные
                    ItemRequest itemRequest;

                    int iCountSend = 0;

                    //mav - проверим на миграцию базу данных
                    Realm realm = Realm.getInstance(activity.getApplicationContext());

                    //mav - перебираем все по очереди
                    for (long l = 0; l < countItem; l++) {
                        //mav - находим одно значение
                        itemRequest = realm.where(ItemRequest.class).equalTo("IsSend", 1).findFirst();

                        //mav - если статус равен 1, значит надо отправить по почте
                        if (itemRequest.getStatus() == 1) {
                            if (sendToMail(itemRequest, realm))
                                iCountSend++;
                        }
                    }
                    realm.close();


                    //mav - теперь обновим наши поля, если хоть что-то отправилось
                    if (iCountSend > 0) {
                        activity.runOnUiThread(() -> RequestsActivity.updateList(lvRequest, activity.getApplicationContext()));
                    }
                }

            } catch (Exception e) {
                Logs.e(TAG, "sendData: ", e);
            }
            //mav - защита от нескольких стартов
            isStart = false;
        });
        thread.setPriority(9);  //mav - минимальный приоритет
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * mav - отправка данных по почте
     *
     * @param itemRequest класс где лежат все данные
     */
    private static boolean sendToMail(ItemRequest itemRequest, Realm realm) {
        boolean b = false;
        try {
            String sMessageBody = "\nФамилия: " + itemRequest.getSurname()
                    + "\nИмя: " + itemRequest.getFirstName()
                    + "\nОтчество: " + itemRequest.getPatronymic()
                    + "\nТелефон: " + itemRequest.getMobilePhone();

            if(itemRequest.getType() == 1)
                sMessageBody = sMessageBody + "\nМЁД";

            String sSubscription = getSubscription(realm);

            //mav - собираем JSON сообщения
            String sJSON = getJSON(itemRequest, sMessageBody + sSubscription);


            String sLink = "localSendEmail-004/";
            String sContentDisposition = "";
            String sBody = sJSON;

            // TODO: 07.04.17 Перенести на боевые сервера
            //mav - отправляем запрос
            String sAnswer = ServerConnector.getResult(sLink, sContentDisposition, sBody, 1);

            if (sAnswer.contains("SUCCESSFULLY")) {
                JSONObject jsonObject = new JSONObject(sAnswer);
                if ("SUCCESSFULLY".equals(jsonObject.get("scriptResponse"))
                        && "Sent".equals(jsonObject.get("resultSendingMessage"))) {
                    realm.beginTransaction();
                    itemRequest.setIsSend(true);
                    realm.commitTransaction();
                    b = true;
                }
            }

            Log.e(TAG, "sendToMail: " + sAnswer);
        } catch (JSONException e) {
            Logs.e(TAG, "sendToMail: ", e);
        }
        return b;
    }

    /**
     * mav - собираем подпись
     */
    private static String getSubscription(Realm realm) {
        String s = "";
        try {

            //mav - читаем наши первые данные
            realm.beginTransaction();
            ItemRegistration itemRegistration = realm.where(ItemRegistration.class).findFirst();
            realm.commitTransaction();

            //mav - собираем строку
            s = "\n\n---------\nМобильное приложение (v" + currentVersion + "):"
                    + "\nФамилия: " + itemRegistration.getSurname()
                    + "\nИмя: " + itemRegistration.getFirstName()
                    + "\nОтчество: " + itemRegistration.getPatronymic()
                    + "\nEmail: " + itemRegistration.getEmail()
                    + "\nТелефон: " + itemRegistration.getMobilePhone();

        } catch (Exception e) {
            Logs.e(TAG, "getSubscription: ", e);
        }

        return s;
    }

    /**
     * mav - собираем JSON
     */
    private static String getJSON(ItemRequest itemRequest, String sMessageBody) {
        String s;
        JSONObject jsonObject = new JSONObject();
        try {
            String FIO = itemRequest.getSurname() + " " + itemRequest.getFirstName() + " " + itemRequest.getPatronymic();
            jsonObject.put("MessageBody", sMessageBody);

            if (itemRequest.getType() == 2) {
                jsonObject.put("MessageTo", "app_mail@ilccredits.com, call@ilccredits.com");
                jsonObject.put("MessageSubject", "Поступила новая заявка - " + FIO);
            }else {
                jsonObject.put("MessageSubject", "Хотят купить МЁД - " + FIO);
                jsonObject.put("MessageTo", "G@merOk.name, am@ilccredits.com");
            }

            s = jsonObject.toString();
        } catch (Exception e) {
            Log.e(TAG, "getJSONfromParams: ", e);
            s = "";
        }
        return s;
    }


}

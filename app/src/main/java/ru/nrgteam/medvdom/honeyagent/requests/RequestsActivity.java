package ru.nrgteam.medvdom.honeyagent.requests;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import io.realm.Sort;
import ru.nrgteam.medvdom.honeyagent.R;
import ru.nrgteam.medvdom.honeyagent.databinding.ARequestsBinding;
import ru.nrgteam.medvdom.honeyagent.global.staticMethods.SF;
import ru.nrgteam.medvdom.honeyagent.global.tools.connector.ServerConnector;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by gamerok on 18.03.17.
 * активити с заявками
 */

public class RequestsActivity extends AppCompatActivity {
    public static final String TAG = "RequestsActivity";

    /**
     * mav - сам биндинг
     */
    private ARequestsBinding binding;

    /**
     * mav - Класс для хранения данных при первоночальной регистрации
     */
    public ItemRequest mItemRequest;

    /**
     * mav - Класс модели обработки биндинга
     */
    public RequestsModel mRequestsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.a_requests);

        //mav - неведанная зверюга!
        new Logs(this);

        //mav - эта строчка уже присудствует в логах
//        //mav - коль работаем с подключением, то инициализируем такую штуку
//        new ServerConnector(getApplicationContext());


        // mav 2017.02.22 - создаём класс для сбора информации по новой заявке
        mItemRequest = new ItemRequest();

        // mav 2017.02.22 - создаём модель по обработки изменений на форме
        mRequestsModel = new RequestsModel(mItemRequest, this);

        // mav 2017.02.22 - соединяем форму с обработкой
        binding.setRequests(mRequestsModel);

        //mav - инициализируем кнопки
        onClickInit();

        //mav - будем отправлять наши заявки
        new RequestConnection(this);

        //mav - оповещаем сервер что мы сюда зашли
        Logs.i(TAG, "onCreate: " + "Started RequestsActivity");
    }

    /**
     * mav - инициализируем кнопки и из обработку
     */
    private void onClickInit() {


        //mav - сохранение заполненных данных
        binding.btnSave.setOnClickListener(view -> btnSaveOnClickListener());

        //mav - отобразить поля для ввода новой заявки
        binding.btnCreate.setOnClickListener(view -> btnCreateOnClickListener(true));

        //mav - скрыть поля для ввода новой заявки
        binding.btnCancel.setOnClickListener(view -> btnCreateOnClickListener(false));

    }

    /**
     * mav - обработчик нажатия на кнопку создания новой заявки
     */
    private void btnCreateOnClickListener(boolean isShowField) {
        if (isShowField) {
            binding.btnCreate.setVisibility(View.GONE);
            binding.llInput.setVisibility(View.VISIBLE);
        } else {
            binding.btnCreate.setVisibility(View.VISIBLE);
            binding.llInput.setVisibility(View.GONE);

            //mav - прячем клавиатуру
            SF.funHideKeyboard(binding.etFirstName, this);
        }
    }


    /**
     * mav - обработчик нажатия на кнопку отправки
     */
    private void btnSaveOnClickListener() {
        try {
            String sErrorText = mRequestsModel.inputValidation();
            if (sErrorText.length() > 0)
                Toast.makeText(RequestsActivity.this, sErrorText, Toast.LENGTH_SHORT).show();
            else
                saveFunction();

        } catch (Exception e) {
            Log.e(TAG, "btnSendOnClickListener: ", e);
        }

    }

    /**
     * mav - список действий при нажатии на кнопку Сохранить и корректных данных
     */
    private void saveFunction() {
        try {
            // mav 2017.03.01 - генерируем новый ключ
            mItemRequest.setUUId();
            mItemRequest.setDateTime();
            mItemRequest.setStatus(1);
            mItemRequest.setIsSend(false);

            //mav - добавляем 7
            long l = 700000000;
            l *= 100;

            mItemRequest.setMobilePhone(mItemRequest.getMobilePhone() + l);

            //mav - спрятать поля для ввода новой заявки
            btnCreateOnClickListener(false);

            //mav - сохраняем в базу данных
            Realm realm = Realm.getInstance(getApplicationContext());

            //mav - если что-то было до этого в базе данных - удаляем и перезаписываем новые данные
            realm.beginTransaction();
            realm.copyToRealm(mItemRequest);
            realm.commitTransaction();
            realm.close();

            //mav - очищаем поля для ввода
            mItemRequest = mRequestsModel.clear();

            //mav - обновление списка
            updateList(binding.lvRequest, getApplicationContext());

            //mav - отправка данных на сервер
            RequestConnection.sendData(binding.lvRequest, this);

        } catch (Exception e) {
            Log.e(TAG, "sendFunction: ", e);
        }

    }

    /**
     * mav - при обновлении экрана проверяем что у нас там есть в базе данных
     */
    @Override
    protected void onResume() {
        super.onResume();

        //mav - обновление списка
        updateList(binding.lvRequest, getApplicationContext());

        //mav - отправка данных на сервер
        RequestConnection.sendData(binding.lvRequest, this);

    }

    /**
     * mav - обновляем лист с нашими заявками
     */
    public static void updateList(ListView lvRequest, Context context) {
        try {
            //mav - сюда будем читать из базы данных
            RealmResults<ItemRequest> iItemRequest;

            //mav - проверим на миграцию базу данных
            Realm realm = Realm.getInstance(context);

            //mav - собственно читаем из базы данных
            iItemRequest = realm.where(ItemRequest.class).findAllSorted("DateTime", Sort.DESCENDING);
            realm.close();

            //mav - если что-то нашли
            if (iItemRequest.size() > 0) {

                //mav - собираем всё в адаптер
                RequestAdapter requestAdapter = new RequestAdapter(iItemRequest, context);

                //mav - и вставляем в наш лист информацию
                lvRequest.setAdapter(requestAdapter);
            }
        } catch (Exception e) {
            Logs.e(TAG, "updateList: ", e);
        }
    }


}

package ru.nrgteam.medvdom.honeyagent.global.tools.connector;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by gamerok on 18.02.17.
 * Класс для работы с дизамическим ключём и сохранением его в базу данных
 */

public class ItemDynamicKey extends RealmObject {
    private static final String TAG = "ItemDynamicKey";

    /**
     * mav - динамический ключ
     */
    @PrimaryKey
    private String Key;

    /**
     * mav - Время создания динамического ключа <p>("2017-02-17 16:59:52.17117")
     */
    private Date DateTime;
    /**
     * mav - Тип ключа <p>(1 - внутренний тест, 2 - внешний тест, 3 - релиз )
     */
    private int Type;
    /**
     * mav - Время последнего запроса нового ключа
     */
    private Date DateConnection;

    public ItemDynamicKey() {
        Key = "00000000-0000-0000-0000-000000000001";
        DateTime = null;
        Type = 0;
        DateConnection = null;
    }


    /**
     * mav - Устанавливаем - динамический ключ <p>("29165661-ea38-4919-bfc6-255bca479e77")
     */
    public void setKey(String dynamicKey) {
        if (dynamicKey != null && dynamicKey.length() == 36)
            Key = dynamicKey;
    }

    /**
     * mav - Читаем - динамический ключ <p>("29165661-ea38-4919-bfc6-255bca479e77")
     */
    public String getKey() {
        return Key;
    }

    /**
     * mav - Устанавливаем - Время создания динамического ключа <p>("2017-02-17 16:59:52.17117")
     */
    public void setDateTime() {
        setDateTime(new Date());
    }

//    /**
//     * mav - Устанавливаем - Время создания динамического ключа <p>("2017-02-17 16:59:52.17117")
//     */
//    @SuppressWarnings("WeakerAccess")
//    public void setDateTime(String dateTime) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS", java.util.Locale.getDefault());
//        try {
//            Date convertedDate = sdf.parse(dateTime);
//            setDateTime(convertedDate);
//        } catch (Exception e) {
//            Log.e(TAG, "setDateTime: ", e);
//        }
//    }

    /**
     * mav - Устанавливаем - Время создания динамического ключа <p>("2017-02-17 16:59:52.17117")
     */
    public void setDateTime(Date dateTime) {
        DateTime = dateTime;
    }


    /**
     * mav - Читаем - Время создания динамического ключа <p>("2017-02-17 16:59:52.17117")
     */
    public Date getDateTime() {
        return DateTime;
    }


    /**
     * mav - Устанавливаем - Тип ключа <p>(1 - внутренний тест, 2 - внешний тест, 3 - релиз )
     */
    public void setType(int type) {
        if (type > 0)
            Type = type;
    }

    /**
     * mav - Читаем - Тип ключа <p>(1 - внутренний тест, 2 - внешний тест, 3 - релиз )
     */
    public int getType() {
        return Type;
    }


    /**
     * mav - Устанавливаем - Время последнего запроса нового ключа
     */
    public void setDateConnection(Date dateConnection) {
        DateConnection = dateConnection;
    }

    /**
     * mav - Устанавливаем - Время последнего запроса нового ключа
     */
    public void setDateConnection() {
        setDateConnection(new Date());
    }

    /**
     * mav - Читаем - Время последнего запроса нового ключа
     */
    public Date getDateConnection() {
        return DateConnection;
    }
}

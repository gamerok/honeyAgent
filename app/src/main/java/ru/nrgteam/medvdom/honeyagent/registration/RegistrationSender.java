package ru.nrgteam.medvdom.honeyagent.registration;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import ru.nrgteam.medvdom.honeyagent.global.staticMethods.SF;
import ru.nrgteam.medvdom.honeyagent.logs.Logs;

/**
 * Created by gamerok on 29.03.17.
 * mav - отправка сообщение с просьбой зарегистрировать приложение через наши скрипты на crm
 */

@SuppressWarnings("WeakerAccess")
public class RegistrationSender {
    private static final String TAG = "RegistrationSender";

    /**
     * mav - стандартная функция отправки данных на сервер
     */
    public static String getResult(String sTextMessage) {
        return getResult(1, sTextMessage);
    }

    /**
     * mav - пробуем отправить письмо сначало на боевой сервак, если не получилось, тогда попробуем на тестовый
     *
     * @param countConnections номер подключения
     * @return результат обработки
     */
    private static String getResult(int countConnections, String sTextMessage) {
        if (countConnections > 4) {
            return "ERROR_countConnections";
        }

        //mav - получаем имя ервера
        String sServerName;
        if (countConnections == 1) {
//            sServerName = "root_ilc_new.connexus.ru"; //mav - боевой сервер
            sServerName = "178.218.119.162"; //mav - боевой сервер
        } else if (countConnections == 2) {
//            sServerName = "test_ilc_new.connexus.ru"; //mav - тестовый сервер
            sServerName = "5.8.177.170"; //mav - тестовый сервер
        }else if (countConnections == 3)
//            sServerName = "test_ilc_new.connexus.ru"; //mav - тестовый сервер
                sServerName = "195.133.234.62"; //mav - тестовый сервер
        else {
//            sServerName = "ILC-L004:8080"; //mav - нахождение сервера внутри сети (по WiFi)
            sServerName = "192.168.7.51:8080"; //mav - нахождение сервера внутри сети (по WiFi)
        }

        //mav - теперь само подключение
        //mav - строка для получения ответа от сервера
        String s = "";

        //mav - куда будем отправлять
        String STRING_URL = "http://" + sServerName + "/" + "crmSendEmail-005" + "/";

        HttpURLConnection httpURLConnection = null;
        try {

            Logs.e(TAG, "getResult: STRING_URL - " + STRING_URL);


            //mav - для начало подготовим URL
            URL url = new URL(STRING_URL);

            //mav - создаём соединение
            httpURLConnection = (HttpURLConnection) url.openConnection();

            //mav - говорим что это POST
            httpURLConnection.setRequestMethod("POST");

            //mav - устанавливаем заголовок для кодировки
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            //mav - устанавливаем заголовок для авторизации
            httpURLConnection.setRequestProperty("Authorization", "VchV4VomPkCP9mY9Im9ziYCRdGeCvzmAEq5vqMzzweBcyUnDZs3mNBto2WZCL3NCV8ZhXTIv1WwDpZUinKh3Dqx8uuqHGFk1YVKUMKixgmQFxEz7KkT2ZHjEVbVyol2h");

            //mav - нужно указать тело сообщения
            OutputStream outputStream = httpURLConnection.getOutputStream();

            //mav - собираем текст сообщения
            String sBody = getJSON("Зарегистрируйте моё приложение (v" + RegistrationApplication.currentVersion + "): \n\n" + sTextMessage);

            //mav - вставляем нашу фотку или другое тело
            outputStream.write(sBody.getBytes());
            outputStream.close();

            //mav - и пробуем отправить
            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());

            //начитываем ответ
            s = SF.readStream(inputStream);
//

        } catch (Exception e) {
            Logs.e(TAG, "getResult: ", e);

            // TODO: 27.04.17
            // mav 2017.03.16 - думаю это пока можно опустить, вроде насмотрелись на это
            // mav 2017.03.16 - не насмотрелись! смотри ещё!!!
            Log.w(TAG, "connectToServer: ", e);
        } finally {
            if (httpURLConnection != null) {
                //Закрываем соединение
                httpURLConnection.disconnect();
            }
        }


        //mav - если ответ положительный то пытаемся прикрепить фотку
        if (s.contains("SUCCESSFULLY")) {

            //noinspection UnnecessaryReturnStatement
            return s;
        } else {

            s = sServerName + ":" + s + ";\n";

            String s1 = getResult(countConnections + 1, sTextMessage);

            if (s1.contains("SUCCESSFULLY"))
                s = s1;
            else
                s = s + s1;

        }

        return s;
    }

    /**
     * mav - собираем JSON
     */
    private static String getJSON(String sMessageBody) {
        String s;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("MessageTo", "app_mail@ilccredits.com");
            jsonObject.put("MessageSubject", "registrations_mobile_app - HoneyAgent");
            jsonObject.put("MessageBody", sMessageBody);

            s = jsonObject.toString();
        } catch (Exception e) {
            Log.e(TAG, "getJSONfromParams: ", e);
            s = "";
        }
        return s;
    }
}

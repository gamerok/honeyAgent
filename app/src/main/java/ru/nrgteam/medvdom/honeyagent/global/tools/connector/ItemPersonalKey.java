package ru.nrgteam.medvdom.honeyagent.global.tools.connector;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by gamerok on 14.03.17.
 * mav - класс для хранения персонального ключа
 */
public class ItemPersonalKey extends RealmObject {
    /**
     * mav - динамический ключ
     */
    @PrimaryKey
    private String Key;

    /**
     * mav - Время создания динамического ключа <p>("2017-02-17 16:59:52.17117")
     */
    private String DataTime;

    public ItemPersonalKey() {
        Key = "00000000-0000-0000-0000-000000000001";
        DataTime = "2000-01-23 01:23:45.678901";
    }


    /**
     * mav - Устанавливаем - динамический ключ <p>("29165661-ea38-4919-bfc6-255bca479e77")
     */
    public void setKey(String dynamicKey) {
        if (dynamicKey != null && dynamicKey.length() == 36)
            Key = dynamicKey;
    }

    /**
     * mav - Читаем - динамический ключ <p>("29165661-ea38-4919-bfc6-255bca479e77")
     */
    public String getKey() {
        return Key;
    }

    /**
     * mav - Устанавливаем - Время создания динамического ключа <p>("2017-02-17 16:59:52.17117")
     */
    public void setDataTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS", java.util.Locale.getDefault());
        setDataTime(sdf.format(new Date()));
    }

    /**
     * mav - Устанавливаем - Время создания динамического ключа <p>("2017-02-17 16:59:52.17117")
     */
    public void setDataTime(String dynamicTime) {
        if (dynamicTime != null && dynamicTime.length() > 18)
            DataTime = dynamicTime;
    }

    /**
     * mav - Читаем - Время создания динамического ключа <p>("2017-02-17 16:59:52.17117")
     */
    public String getDataTime() {
        return DataTime;
    }

}

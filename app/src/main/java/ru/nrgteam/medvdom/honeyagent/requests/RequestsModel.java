package ru.nrgteam.medvdom.honeyagent.requests;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.Color;
import android.widget.RadioGroup;

import io.realm.Realm;
import ru.nrgteam.medvdom.honeyagent.BR;
import ru.nrgteam.medvdom.honeyagent.R;

/**
 * Created by gamerok on 18.03.17.
 * модель поведения активизи с заявками
 */

@SuppressWarnings({"WeakerAccess", "UnusedParameters"})
public class RequestsModel extends BaseObservable {

    /**
     * mav - Класс содержимого
     */
    private ItemRequest mItemRequest;

    /**
     * mav - Контекст
     */
    private Context mContext;

    // mav 2017.02.22 - инициализация переменных этого класса
    public RequestsModel(ItemRequest itemRequest, Context context) {
        mItemRequest = itemRequest;
        mContext = context;
    }

    /**
     * mav - очищаем данные из нашего класса
     *
     */
    public ItemRequest clear() {
        mItemRequest = new ItemRequest();
        notifyPropertyChanged(BR.surname);
        notifyPropertyChanged(BR.firstName);
        notifyPropertyChanged(BR.patronymic);
        notifyPropertyChanged(BR.mobilePhone);
        notifyPropertyChanged(BR.type);
        notifyPropertyChanged(BR.saveColor);
        return mItemRequest;
    }

    /**
     * mav - Читаем - значение поля Фамилия <p>("Мятечкин")
     */
    @Bindable
    public String getSurname() {
        return mItemRequest.getSurname();
    }

    /**
     * mav - Читаем - значение поля Имя <p>("Александр")
     */
    @Bindable
    public String getFirstName() {
        return mItemRequest.getFirstName();
    }

    /**
     * mav - Читаем - значение поля Отчество <p>("Викторович")
     */
    @Bindable
    public String getPatronymic() {
        return mItemRequest.getPatronymic();
    }

    /**
     * mav - Читаем - значение поля Мобильный телефон <p>("9103089229")
     */
    @Bindable
    public String getMobilePhone() {
        if (mItemRequest.getMobilePhone() > 0)
            return String.valueOf(mItemRequest.getMobilePhone());
        return "";
    }

    /**
     * mav - Читаем - Тип заявки <p>(1 - мёд, 2 - деньги)
     */
    @Bindable
    public int getType() {
        return mItemRequest.getType();
    }

    /**
     * mav - действия при добавлении симфолов в Фамилию
     */
    public void onSurnameChanged(CharSequence s, int start, int before, int count) {
        mItemRequest.setSurname("" + s);
        notifyPropertyChanged(BR.surname);
        notifyPropertyChanged(BR.saveColor);
    }

    /**
     * mav - действия при добавлении симфолов в Имя
     */
    public void onFirstNameChanged(CharSequence s, int start, int before, int count) {
        mItemRequest.setFirstName("" + s);
        notifyPropertyChanged(BR.firstName);
        notifyPropertyChanged(BR.saveColor);
    }

    /**
     * mav - действия при добавлении симфолов в Отчество
     */
    public void onPatronymicChanged(CharSequence s, int start, int before, int count) {
        mItemRequest.setPatronymic("" + s);
        notifyPropertyChanged(BR.patronymic);
    }

    /**
     * mav - действия при добавлении симфолов в Мобильный телефон
     */
    public void onMobilePhoneChanged(CharSequence s, int start, int before, int count) {
        mItemRequest.setMobilePhone("" + s);
        notifyPropertyChanged(BR.mobilePhone);
        notifyPropertyChanged(BR.saveColor);
    }

    /**
     * mav - действия при добавлении симфолов в Мобильный телефон
     */
    public void onTypeChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case -1:
                mItemRequest.setType(0);
                break;
            case R.id.rbR_Type_Honey:
                mItemRequest.setType(1);
                break;
            case R.id.rbR_Type_Money:
                mItemRequest.setType(2);
                break;
        }
        notifyPropertyChanged(BR.type);
        notifyPropertyChanged(BR.saveColor);
    }


    /**
     * mav - проверяем на корректность ввода наши поля
     */
    public String inputValidation() {
        String text = "";
        if (getSurname().trim().length() <= 1)
            text = "Введите фамилию";
        else if (getFirstName().trim().length() <= 1)
            text = "Введите имя";
        else if (getMobilePhone().length() != 10)
            text = "Введите мобильный телефон";
        else if (getType() == 2) {
            //mav - сохраняем в базу данных
            Realm realm = Realm.getInstance(mContext);

            //mav - добавляем 7
            long l7 = 700000000;
            l7 *= 100;

            //mav - если что-то было до этого в базе данных - удаляем и перезаписываем новые данные
            long l = realm.where(ItemRequest.class).equalTo("MobilePhone", mItemRequest.getMobilePhone() + l7).equalTo("Type", mItemRequest.getType()).count();
            realm.close();

            if (l > 0) {
                text = "Клиент с таким-же мобильным телефоном уже есть";
            }
        }
        return text;
    }

    /**
     * mav - Определяем цвет текста кнопки "Отпарвить"
     */
    @Bindable
    public int getSaveColor() {
        if (inputValidation().length() > 0)
            return Color.RED;
        return Color.GREEN;
    }


}

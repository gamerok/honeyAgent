 Command line instructions
Git global setup

git config --global user.name "Alexander"
git config --global user.email "g@merok.name"

Create a new repository

git clone https://gamerok@gitlab.com/gamerok/honeyAgent.git
cd honeyAgent
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gamerok@gitlab.com/gamerok/honeyAgent.git
git add .
git commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gamerok@gitlab.com/gamerok/honeyAgent.git
git push -u origin --all
git push -u origin --tags
